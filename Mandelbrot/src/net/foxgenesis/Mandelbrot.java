package net.foxgenesis;

import net.foxgenesis.FractalViewer.Fractal;

public class Mandelbrot extends Fractal {

	public Mandelbrot() {
		super(0,-1,24);
	}
	
	@Override
	public int fractal(double dx, double dy) {
		double zx = 0.0, zy = 0.0;
		double zx2 = 0.0, zy2 = 0.0;
		int value = 0;
		while (value < getMax() && zx2 + zy2 < 4.0) {
			zy = 2.0 * zx * zy + dx;
			zx = zx2 - zy2 + dx;
			zx2 = zx * zx;
			zy2 = zy * zy;
			value++;
		}
		return value == getMax() ? 0 : value;
	}
	
}