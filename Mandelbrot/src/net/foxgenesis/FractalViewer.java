package net.foxgenesis;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FractalViewer extends JApplet implements MouseListener, MouseMotionListener {
	private static final long serialVersionUID = 7646215930407163393L;
	private int colorIter = 200;
	private double viewX = 0.0;
	private double viewY = 0.0;
	private double zoom = 1.0;
	private int mouseX,dragX,x;
	private int mouseY,dragY,y;
	private BufferedImage image;
	private boolean updateImg = true;
	private Fractal f;

	public static void main(String[] args) {
		JFrame frame = new JFrame("Fractal Viewer");
		frame.setSize(500,500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new FractalViewer(new TestFractal()));
		frame.setVisible(true);
	}
	public static abstract class Fractal {
		private double cx,cy;
		private long max = 150000000;
		private Color[] colors;
		public Fractal() {
			this(-0.74543,0.11301);
		}
		public Fractal(double cx, double cy) {
			this(cx,cy,9000);
		}
		
		public Fractal(double cx, double cy, int colorsAmt) {
			this.cx = cx;
			this.cy = cy;
			colors = new Color[colorsAmt];
			for (int i = 0; i < colors.length; i++) {
				int c = 2 * i * 256 / colors.length;
				if (c > 255)
					c = 511 - c;
				colors[i] = new Color(c, c, c);
			}
		}
		
		public long getMax() {
			return max;
		}
		public Color getColor(int i) {
			return colors[i];
		}
		public int getColors() {
			return colors.length;
		}
		public double getCX() {
			return cx;
		}
		public double getCY() {
			return cy;
		}
		public abstract int fractal(double dx, double dy);
	}
	
	public FractalViewer() {
		this(new JuliaSet());
	}
	
	public FractalViewer(Fractal f) {
		this(f,9000);
	}

	public FractalViewer(Fractal f, int colorsAmt) {
		this.f = f;
		addMouseListener(this);
		addMouseMotionListener(this);
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		setGlassPane(panel);
	}

	public void mousePressed(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
	}

	public void mouseReleased(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		Graphics g = this.getGraphics();
		g.setColor(Color.GREEN);
		g.setColor(g.getColor().brighter());
		g.drawString("Loading...", this.getWidth()/2, this.getHeight()/2);
		if ((e.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
			if (x != mouseX && y != mouseY) {
				int w = getSize().width;
				int h = getSize().height;
				viewX += zoom * Math.min(x, mouseX) / Math.min(w, h);
				viewY += zoom * Math.min(y, mouseY) / Math.min(w, h);
				zoom *= Math.max((double)Math.abs(x - mouseX) / w, (double)Math.abs(y - mouseY) / h);
			}
		}
		getGlassPane().repaint();
		updateImg = true;
		repaint();
	}

	@Override
	public void paint(Graphics g) {
		int width = this.getWidth();
		int height = this.getHeight();
		if(updateImg) {
			image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			for (int y = 0; y < height; y++) 
				for (int x = 0; x < width; x++) {
					double r = zoom / Math.min(width, height);
					double dx = 2.5 * (x * r + viewX) - 2.0;
					double dy = 1.25 - 2.5 * (y * r + viewY);
					int value = f.fractal(dx, dy);
					g.setColor(f.getColor(value % f.getColors()));
					image.setRGB(x, y, value | (value << colorIter));
				}
			updateImg = false;
		}
		g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		String s = "Zoom: " + zoom;
		Rectangle2D r = g.getFont().getStringBounds(s, g.getFontMetrics().getFontRenderContext());
		g.setColor(new Color(0f,0f,0f,.8f));
		g.fillRect(18, 30-(int)r.getHeight()+3, (int)r.getWidth()+2, (int)r.getHeight());
		g.setColor(Color.GREEN.brighter());
		g.drawString(s, 20, 30);
		g.drawLine(x-5, y, x+5, y);
		g.drawLine(x, y-5, x, y+5);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		this.getGraphics().drawImage(image, 0, 0, this);
		dragX = e.getX();
		dragY = e.getY();
		Graphics g = getGlassPane().getGraphics();
		g.setColor(Color.GREEN);
		g.drawRect(mouseX, mouseY, dragX-mouseX, dragY-mouseY);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		x = e.getX();
		y = e.getY();
		repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
}
