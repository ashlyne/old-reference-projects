package net.foxgenesis;

import net.foxgenesis.FractalViewer.Fractal;

public class TestFractal extends Fractal{
	public TestFractal() {
		super(-0.8,0.156);
	}
	@Override
	public int fractal(double zx, double zy) {
		for (int i = 0; i < getColors(); i++) {
			double newx = zx * zx - zy * zy + getCX();
			double newy = 2.2 * zx * zy + getCY();
			zx = newx;
			zy = newy;
			if ((zx * zx) - (zy * zy) > 80)
				return i;
		}
		return getColors()- 1;
	}

}
