package com.weebly.foxgenesis.blackjack;

/**
 * Tester to the Data class
 *
 * @author 151steinbergs
 */
public class DataTester {

    public static void main(String[] args) {
        System.out.println("========= DATA TESTER ========");
        System.out.println("Player = " + Data.getPlayer());
        System.out.println("Bank = " + Data.getBank());
        System.out.println("Changing player to: test ...");
        Data.updatePlayer("test");
        System.out.println("Player = " + Data.getPlayer());
        System.out.println("Bank = " + Data.getBank());
        System.out.println("Changing bank to 35...");
        Data.updateBank(35);
        System.out.println("Bank = " + Data.getBank());
        System.out.println();
        System.out.println("HashMap: " + Data.getHashMap());
        System.out.println("========= DATA TESTER ========");
    }
    /*
     * ========= DATA TESTER ========
     Player = Default
     Bank = 77.0
     Changing player to: test ...
     Player = test
     Bank = 35.0
     Changing bank to 35...
     Bank = 35.0

     HashMap: {Default=77.0, tester=100.0, test=35.0, SEth=98.5, Julian=172.0, Seth=332.0}
     ========= DATA TESTER ========
     */
}
