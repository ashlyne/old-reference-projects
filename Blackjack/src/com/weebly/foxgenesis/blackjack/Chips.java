package com.weebly.foxgenesis.blackjack;

import org.newdawn.slick.Image;

public enum Chips {

    RED(100),
    BLUE(10),
    PURPLE(1000),
    GOLD(10000),
    GREEN(1);
    private int value = 0;
    private Image image;

    Chips(int value) {
        this.value = value;
        try {
            image = new Image("/textures/" + this.name().toLowerCase() + "chip.png");
        } catch (Exception e) {
        }
    }

    /**
     * Returns the value of the chip
     *
     * @return int
     */
    public int getValue() {
        return value;
    }

    /**
     * Returns the image of the chip
     *
     * @return Image
     */
    public Image getImage() {
        return image;
    }
}
