package com.weebly.foxgenesis.blackjack;

import java.util.Random;

/**
 *
 * @author 151steinbergs
 */
public class CardTester {

    public static void main(String[] args) {
        System.out.println("============ CARD TESTER ===========");
        String[] suits = {"S", "C", "H", "D"};
        for (String a : suits) {
            Card card = new Card((new Random().nextInt(13) + 1) + ":" + a);
            System.out.println(card);
            System.out.println("setting showing to false...");
            card.setShowing(false);
            System.out.println(card);
            System.out.println("setting value to 11...");
            card.setValue(11);
            System.out.println(card);
        }
        System.out.println("============ CARD TESTER ===========");
    }
    /*
     * ============ CARD TESTER ===========
     created card[9:S]
     Card{delim=9:S; value=9; rank=9; suit=s; draggable=false; showing=true}
     setting showing to false...
     Card{delim=9:S; value=9; rank=9; suit=s; draggable=false; showing=false}
     setting value to 11...
     Card{delim=9:S; value=11; rank=9; suit=s; draggable=false; showing=false}
     created card[13:C]
     Card{delim=13:C; value=13; rank=13; suit=c; draggable=false; showing=true}
     setting showing to false...
     Card{delim=13:C; value=13; rank=13; suit=c; draggable=false; showing=false}
     setting value to 11...
     Card{delim=13:C; value=11; rank=13; suit=c; draggable=false; showing=false}
     created card[7:H]
     Card{delim=7:H; value=7; rank=7; suit=h; draggable=false; showing=true}
     setting showing to false...
     Card{delim=7:H; value=7; rank=7; suit=h; draggable=false; showing=false}
     setting value to 11...
     Card{delim=7:H; value=11; rank=7; suit=h; draggable=false; showing=false}
     created card[8:D]
     Card{delim=8:D; value=8; rank=8; suit=d; draggable=false; showing=true}
     setting showing to false...
     Card{delim=8:D; value=8; rank=8; suit=d; draggable=false; showing=false}
     setting value to 11...
     Card{delim=8:D; value=11; rank=8; suit=d; draggable=false; showing=false}
     ============ CARD TESTER ===========
     */
}
