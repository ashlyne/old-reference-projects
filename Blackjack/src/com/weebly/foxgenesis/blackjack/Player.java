package com.weebly.foxgenesis.blackjack;

import javax.swing.JOptionPane;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

/**
 * Player class used for blackjack
 *
 * @author Seth Player.java
 */
public class Player extends Container {

    private Button stand;
    private Button hit;
    private Button[] chips;
    private Button deal;
    private Hand hand;
    private Button dd;
    private boolean turn = true;
    private int value = 0;
    private boolean hasAce = false;
    private boolean haveButtons = true;
    private Button clear;
    private MoveHandler handler;
    private double bet = 1;
    private boolean doubled = false;

    /**
     * Create a new blackjack player
     *
     * @param input - Input for the buttons
     * @param handler - Move handler for buttons
     */
    public Player(Input input, final MoveHandler handler) {
        this.handler = handler;

        stand = new Button("Stand");
        stand.listen(input);
        stand.setAction(new Action() {
            @Override
            public void act(Input input) {
                stand();
            }
        });

        hit = new Button("Hit");
        hit.listen(input);
        hit.setAction(new Action() {
            @Override
            public void act(Input input) {
                hit();
            }
        });

        deal = new Button("Deal");
        deal.listen(input);
        deal.setAction(new Action() {
            @Override
            public void act(Input input) {

                if (hand.getCards().isEmpty());
                deal();
            }
        });

        clear = new Button("Clear Bet");
        clear.listen(input);
        clear.setAction(new Action() {
            @Override
            public void act(Input input) {
                setBet(1);
            }
        });

        dd = new Button("Double Down");
        dd.listen(input);
        dd.setAction(new Action() {
            @Override
            public void act(Input input) {
                if (hand.getCards().size() == 2) {
                    doubleDown();
                }
            }
        });

        hand = new Hand();
        chips = new Button[5];

        for (int i = 0; i < chips.length; i++) {
            chips[i] = new Button();
            Chips chip = null;
            switch (i) {
                case 0:
                    chip = Chips.GREEN;
                    break;
                case 1:
                    chip = Chips.BLUE;
                    break;
                case 2:
                    chip = Chips.RED;
                    break;
                case 3:
                    chip = Chips.PURPLE;
                    break;
                case 4:
                    chip = Chips.GOLD;
                    break;
            }
            chips[i].setAnimation(new Animation(new Image[]{chip.getImage(), chip.getImage()}, new int[]{300, 300}, false));
            chips[i].setSize(60, 60);
            chips[i].drawShadow(false);
            chips[i].setDrawBorder(false);
            chips[i].setToolTipText("$" + chip.getValue());
            createChipListener(chip, chips[i], input);
        }
    }

    private void doubleDown() {
        if (turn) {
            doubled = true;
            handler.doubleDown(this);
        }
    }

    /**
     * returns the players hand of cards
     *
     * @return the players hand
     */
    public Hand hand() {
        return hand;
    }

    /**
     * Checks if it is the players turn
     *
     * @return players turn
     */
    public boolean isPlayersTurn() {
        return turn;
    }

    /**
     * tells the handler to make this player stand
     */
    public void stand() {
        if (isPlayersTurn()) {
            handler.stand(this);
        }
    }

    /**
     * Sets the bet of the player
     *
     * @param amount - bet amount
     */
    public void setBet(double amount) {
        if (amount >= Data.getBank()) {
            this.bet = Data.getBank();
        } else {
            this.bet = amount;
        }
    }

    /**
     * Returns the bet the player has chosen
     *
     * @return players bet
     */
    public double getBet() {
        return bet;
    }

    /**
     * Returns the amount of money this player has
     *
     * @return amount of money
     */
    public double getMoney() {
        return Data.getBank();
    }

    /**
     * Called by the button, asks the handler to deal this player cards
     */
    private void deal() {
        handler.deal(this);
    }

    /**
     * Updates the max amount of card the player can have
     *
     * @param amount - amount to set
     */
    private void updateCards(int amount) {
        hand.setMaxCards(hand.getCards().size() + amount);
    }

    /**
     * give the player a special hand
     *
     * @param hand - hand to assign
     */
    public void setHand(Hand hand) {
        this.hand = hand;
    }

    /**
     * Gets the numeric value of the players hand
     *
     * @return int
     */
    public int getValue() {
        return value;
    }

    /**
     * returns weather or not the player has an ace
     *
     * @return boolean
     */
    public boolean hasAce() {
        return hasAce;
    }

    /**
     * Sets whether the buttons should be drawn for this player
     *
     * @param state
     */
    public void haveButtons(boolean state) {
        this.haveButtons = state;
    }

    /**
     * Resets the player for a new round
     */
    public void reset() {
        turn = true;
        value = 0;
        update();
        hand.setMaxCards(0);
        if (doubled) {
            setBet(getBet() / 2);
            doubled = false;
        }
        if (getMoney() < getBet()) {
            setBet(getMoney());
        }
        if (getMoney() <= 0) {
            JOptionPane.showMessageDialog(null, "Ooops! You lost\nall your money!\nThe house will lend you $100 to keep playing.\nHave fun!", "Aww", JOptionPane.WARNING_MESSAGE);
            Data.updateBank(100);
            setBet(1);
        }
    }

    /**
     * Called by the dealer The dealer deals the player cards
     *
     * @param deck - Deck to take cards from
     */
    public void dealCards(Shoe deck) {
        updateCards(2);
        deck.moveCard(deck.getCards().get(0), hand());
        deck.moveCard(deck.getCards().get(0), hand());
        updateValue();
        if (value == 21 && !(this instanceof Dealer)) {
            handler.blackjack(this);
        }
    }

    private void showChips(boolean state) {
        for (Button a : chips) {
            a.setVisible(state);
            a.setEnabled(state);
        }
        clear.setVisible(state);
        clear.setEnabled(state);
    }

    /**
     * Hits the player (deals them a card)
     */
    public void hit() {
        if (isPlayersTurn()) {
            updateCards(1);
            handler.hit(this);
            updateValue();
            if (getValue() > 21) {
                handler.bust(this);
            }
            if (getValue() == 21) {
                handler.blackjack(this);
            }
        }
    }

    private void createChipListener(final Chips chip, Button button, Input input) {
        button.listen(input);
        button.setAction(new Action() {
            @Override
            public void act(Input input) {
                setBet(getBet() + chip.getValue());
            }
        });
    }

    /**
     * Sets the players turn
     *
     * @param state - state of players turn
     */
    public void setPlayersTurn(boolean state) {
        turn = state;
        if (!turn) {
            stand.setEnabled(false);
            hit.setEnabled(false);
        } else {
            stand.setEnabled(true);
            hit.setEnabled(true);
        }

    }

    /**
     * Updates the value of the player
     */
    public void updateValue() {
        int temp = 0;
        for (Card a : hand.getCards()) {
            if (this instanceof Dealer) {
                if (a.isShowing()) {
                    int rank = a.getRank();
                    if (rank == 11 || rank == 12 || rank == 13) {
                        temp += 10;
                    } else if (rank == 1) {
                        hasAce = true;
                        rank = 11;
                        temp += rank;
                    } else {
                        temp += rank;
                    }

                } else;
            } else {
                int rank = a.getRank();
                if (rank == 11 || rank == 12 || rank == 13) {
                    temp += 10;
                } else if (rank == 1) {
                    hasAce = true;
                    rank = 11;
                    temp += rank;
                } else {
                    temp += rank;
                }
            }

        }
        if (temp > 21 && hasAce) {
            for (Card a : hand().getCards()) {
                if (a.getRank() == 1) {
                    temp -= 10;
                }
                if (temp <= 21) {
                    break;
                }
            }
        }
        this.value = temp;
    }

    /**
     * Draws the player
     *
     * @param g - Graphics to draw with
     */
    public void draw(Graphics g) {
        update();
        if (haveButtons) {
            stand.draw(g);
            hit.draw(g);
            deal.draw(g);
            for (Button a : chips) {
                a.draw(g);
            }
            g.setColor(Color.white);
            g.drawString("Bank:\n$" + Data.getBank(), (int) getX() + getWidth() - 30, (int) getY() + getHeight() + 10);
            g.drawString("Bet: \n$" + bet, (int) getX() + getWidth() - 30, (int) getY() + getHeight() + 50);
            if (hand.getCards().size() == 2) {
                dd.draw(g);
            }
            clear.draw(g);
        }
        hand.draw(g);
        g.setColor(Color.white);
        if (hand.getCards().size() > 0) {
            g.drawString("Value: " + getValue(), (int) hit.getX() + hit.getWidth() + 30, (int) getY() - 35);
        }
    }

    /**
     * Updates Button Locations
     */
    private void updateLocations() {
        hand.getContainer().setLocation((int) stand.getX(), (int) this.getY());
        if (haveButtons) {
            stand.setLocation(this.getX() - (this.getWidth() + 30), this.getY() - 40);
            hit.setLocation(this.getX() - (this.getWidth() + 40) + stand.getWidth() + 20, this.getY() - 40);
            deal.setLocation(this.getX() - (this.getWidth() + 50) + hit.getWidth() + 20, this.getY() - 40);

            for (int i = 1; i < chips.length + 1; i++) {
                chips[i - 1].setLocation((this.getX() - 30) + (i * chips[i - 1].getWidth()), this.getY() - 100);
            }
            clear.setLocation(this.getX() + 40, this.getY() + 60);

            dd.setLocation(stand.getX(), this.getY() - 80);
        }
    }

    /**
     * Method called to update buttons and player values
     */
    private void update() {
        updateLocations();
        if (hand.getCards().size() > 0 && turn) {
            deal.setVisible(false);
            deal.setEnabled(false);
            stand.setEnabled(true);
            stand.setVisible(true);
            hit.setVisible(true);
            hit.setEnabled(true);
            showChips(false);
        } else if (hand.getCards().isEmpty()) {
            deal.setVisible(true);
            deal.setEnabled(true);
            stand.setEnabled(false);
            stand.setVisible(false);
            hit.setVisible(false);
            hit.setEnabled(false);
            hasAce = false;
            value = 0;
            showChips(true);
        } else if (hand.getCards().size() > 0 && !turn) {
            deal.setVisible(false);
            deal.setEnabled(false);
            stand.setEnabled(false);
            stand.setVisible(true);
            hit.setVisible(true);
            hit.setEnabled(false);
            showChips(false);
        }
        if (hand.getCards().size() == 2 && turn) {
            dd.setVisible(true);
            dd.setEnabled(true);
        } else {
            dd.setEnabled(false);
            dd.setVisible(false);
        }
    }

    /**
     * String representation of the player
     */
    @Override
    public String toString() {
        String text = "Player{Players Turn = " + turn;
        text += "; Cards = " + hand.getCards().size();
        text += "; Max Cards = " + hand.getMaxCards();
        text += "; In Round = " + (hand.getCards().size() > 0);
        text += "; Value = " + value;
        text += "; Has Ace = " + hasAce;
        text += "; Money = " + getMoney();
        text += "; Bet = " + getBet();
        if (this.haveButtons) {
            text += "; Stand{visible = " + stand.isVisible() + ", enabled = " + stand.isEnabled() + "}";
            text += "; Deal{visible = " + deal.isVisible() + ", enabled = " + deal.isEnabled() + "}";
            text += "; Hit{visible = " + hit.isVisible() + ", enabled = " + hit.isEnabled() + "}";
        }
        return text + "}";
    }
}

interface MoveHandler {

    /**
     * Called when a player asked to be dealt cards
     *
     * @param player
     */
    public void deal(Player player);

    /**
     * Called when a player wants to stand
     *
     * @param player Player to stand
     */
    public void stand(Player player);

    /**
     * Called when a player wants to be hit
     *
     * @param player Player to be hit
     */
    public void hit(Player player);

    /**
     * Called when a player goes over 21
     *
     * @param player Player that went over
     */
    public void bust(Player player);

    /**
     * Called when a player is dealt blackjack
     *
     * @param player Player that got blackjack
     */
    public void blackjack(Player player);

    /**
     * Called when a player wants to double down
     *
     * @param player Player to double down
     */
    public void doubleDown(Player player);

    /**
     * Called when a player wants to get insurance
     *
     * @param player Player that wants insurance
     */
    public void getInsurance(Player player, boolean wants);
}
