package com.weebly.foxgenesis.blackjack;

import org.newdawn.slick.Input;

/**
 *
 * @author 151steinbergs
 */
public class PlayerTester {

    public static void main(Input input) {
        System.out.println("============ PLAYER TESTER ==============");
        Player player = new Player(input, new MoveHandler() {
            @Override
            public void deal(Player player) {
            }

            @Override
            public void stand(Player player) {
            }

            @Override
            public void hit(Player player) {
            }

            @Override
            public void bust(Player player) {
            }

            @Override
            public void blackjack(Player player) {
            }

            @Override
            public void doubleDown(Player player) {
            }

            @Override
            public void getInsurance(Player player, boolean wants) {
            }
        });
        System.out.println(player);
        System.out.println("setting bet to 100...");
        player.setBet(100);
        System.out.println(player);
        System.out.println("setting players turn to false...");
        player.setPlayersTurn(false);
        System.out.println(player);
        System.out.println("============ PLAYER TESTER ==============");
    }
    /*
     * ============ PLAYER TESTER ==============
     Player{Players Turn = true; Cards = 0; Max Cards = -1; In Round = false; Value = 0; Has Ace = false; Money = 100.0; Bet = 1.0; Stand{visible = true, enabled = true}; Deal{visible = true, enabled = true}; Hit{visible = true, enabled = true}}
     setting bet to 100...
     Player{Players Turn = true; Cards = 0; Max Cards = -1; In Round = false; Value = 0; Has Ace = false; Money = 100.0; Bet = 100.0; Stand{visible = true, enabled = true}; Deal{visible = true, enabled = true}; Hit{visible = true, enabled = true}}
     setting players turn to false...
     Player{Players Turn = false; Cards = 0; Max Cards = -1; In Round = false; Value = 0; Has Ace = false; Money = 100.0; Bet = 100.0; Stand{visible = true, enabled = false}; Deal{visible = true, enabled = true}; Hit{visible = true, enabled = false}}
     ============ PLAYER TESTER ==============
     */
}
