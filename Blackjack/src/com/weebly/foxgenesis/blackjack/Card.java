package com.weebly.foxgenesis.blackjack;

import java.util.HashMap;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Point;

/**
 * Card component that resembles a playing card
 *
 * @author 151steinbergs
 */
public class Card extends Button {

    private int rank;
    private String suit;
    private int value = -1;
    private CardAnimations pic;
    private final String s;
    private boolean flipped = true;
    private boolean draggable = false;

    /**
     * Create a new card with a given string example: "10S" 10S will create a
     * card with the number of 10 and the suit of spades
     *
     * @param str A string that creates the card
     * @throws SlickException
     */
    public Card(String str) {
        super("");
        String[] temp = str.split(":");
        rank = Integer.parseInt(temp[0].toLowerCase());
        suit = temp[1].toLowerCase();
        s = str;
        pic = new CardAnimations(str);
        this.setAnimation(pic.getImage());
        System.out.println("created card[" + s + "]");
        this.setSize(CardPicUtils.CARD_WIDTH, CardPicUtils.CARD_HEIGHT);
        value = rank;
    }

    /**
     * Gets the string representation the cards rank and suit
     *
     * @return String
     */
    public String getCardDelimiter() {
        return s;
    }

    /**
     * Sets the value of the card
     *
     * @param value value of the card
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Gets the value of the card
     *
     * @return int
     */
    public int getValue() {
        return value;
    }

    /**
     * Gets the ranking of the card
     *
     * @return int
     */
    public int getRank() {
        return rank;
    }

    /**
     * Gets the suit of the card
     *
     * @return String
     */
    public String getSuit() {
        return suit;
    }

    /**
     * Set weather the card is able to be dragged across the screen
     *
     * @param state
     */
    public void setDraggable(boolean state) {
        draggable = state;
    }

    /**
     * Returns weather the card is able to be dragged across the screen
     *
     * @return boolean
     */
    public boolean isDraggable() {
        return draggable;
    }

    /**
     * Adds a drag listener for the card. this is used if you want the card to
     * be able to be dragged across the screen
     *
     * @param input
     */
    public void addDragListener(Input input) {
        input.addMouseListener(new MouseListener() {
            @Override
            public void inputEnded() {
            }

            @Override
            public void inputStarted() {
            }

            @Override
            public boolean isAcceptingInput() {
                return true;
            }

            @Override
            public void setInput(Input input) {
            }

            @Override
            public void mouseClicked(int button, int x, int y, int clickCount) {
            }

            @Override
            public void mouseDragged(int oldx, int oldy, int newx, int newy) {
                if (draggable && contains(oldx, oldy)) {
                    setLocation(newx, newy);
                }
            }

            @Override
            public void mouseMoved(int oldx, int oldy, int newx, int newy) {
            }

            @Override
            public void mousePressed(int button, int x, int y) {
            }

            @Override
            public void mouseReleased(int button, int x, int y) {
            }

            @Override
            public void mouseWheelMoved(int change) {
            }
        });
    }

    /**
     * Sets weather the is flipped.
     *
     * @param state
     */
    public void setShowing(boolean state) {
        if (state) {
            this.flipped = true;
            this.setAnimation(pic.getImage());
        } else {
            this.flipped = false;
            this.setAnimation(pic.getBack());
        }
    }

    /**
     * Returns true if the front of the card is showing
     *
     * @return boolean
     */
    public boolean isShowing() {
        return flipped;
    }

    /**
     * Gets the string representation of the card
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Card{delim=" + s + "; value=" + value + "; rank=" + rank + "; suit=" + suit + "; draggable=" + isDraggable() + "; showing=" + isShowing() + "}";
    }
}

class CardAnimations {

    private Image image;
    private Image back;

    /**
     * Create a new CardAnimations with given card
     *
     * @param delim String representation of the card
     */
    public CardAnimations(String delim) {
        try {
            image = CardPicUtils.getCardImage(delim);
            back = CardPicUtils.getCardBack();
        } catch (Exception e) {
        }
    }

    /**
     * Gets the cards front image
     *
     * @return Animation
     */
    public Animation getImage() {
        return new Animation(new Image[]{image, image}, new int[]{300, 300}, false);
    }

    /**
     * Gets the image for the back of the card
     *
     * @return Animation
     */
    public Animation getBack() {
        if(Data.getPlayer().equalsIgnoreCase("rick"))
            return new Animation(RickRoll.rick, RickRoll.time, true);
        return new Animation(new Image[]{back, back}, new int[]{300, 300}, false);
    }
}

final class CardPicUtils {

    private static Image image;
    public static final int CARD_WIDTH = 72;
    public static final int CARD_HEIGHT = 96;
    public static int IMAGE_WIDTH;
    public static int IMAGE_HEIGHT;
    private static Image back;
    public static Image cursor;

    static {
        try {
            image = new Image("/textures/Cards.png");
            back = new Image("/textures/cardBack.png").getScaledCopy(CARD_WIDTH, CARD_HEIGHT);
            cursor = new Image("/textures/cursor.png");
            IMAGE_WIDTH = CardPicUtils.image.getWidth();
            IMAGE_HEIGHT = CardPicUtils.image.getHeight();
        } catch (Exception e) {
        }
    }

    /**
     * Returns the card image
     *
     * @param delim String representation of the card
     * @return Image
     */
    public static Image getCardImage(String delim) {
        int rank;
        String suit;
        String[] temp = delim.split(":");

        switch (temp[0].toLowerCase()) {
            case "a":
                rank = 1;
                break;
            case "k":
                rank = 13;
                break;
            case "q":
                rank = 12;
                break;
            case "j":
                rank = 11;
                break;
            default:
                rank = Integer.parseInt(temp[0]);
                break;
        }

        suit = temp[1].toLowerCase();

        Locations loc = null;
        switch (suit) {
            case "d":
                loc = Locations.Diamonds;
                break;
            case "h":
                loc = Locations.Hearts;
                break;
            case "c":
                loc = Locations.Clubs;
                break;
            case "s":
                loc = Locations.Spades;
                break;
        }
        Point point = loc.getPoint(rank);
        return image.getSubImage((int) point.getX(), (int) point.getY(), CARD_WIDTH, CARD_HEIGHT);
    }

    /**
     * Gets the image for the back of the card
     *
     * @return Image
     */
    public static Image getCardBack() {
        return back;
    }
}

enum Locations {

    Diamonds(295),
    Clubs(1),
    Spades(99),
    Hearts(197);
    private HashMap<Integer, Point> map;

    Locations(int y) {
        map = new HashMap<>();
        map.put(1, new Point(1, y));
        map.put(2, new Point(74, y));
        map.put(3, new Point(147, y));
        map.put(4, new Point(220, y));
        map.put(5, new Point(293, y));
        map.put(6, new Point(366, y));
        map.put(7, new Point(439, y));
        map.put(8, new Point(512, y));
        map.put(9, new Point(585, y));
        map.put(10, new Point(658, y));
        map.put(11, new Point(731, y));
        map.put(12, new Point(804, y));
        map.put(13, new Point(877, y));
    }

    /**
     * Returns the top left corner of the card
     *
     * @param rank Cards rank
     * @return Point
     */
    public Point getPoint(int rank) {
        return map.get(rank);
    }
}
