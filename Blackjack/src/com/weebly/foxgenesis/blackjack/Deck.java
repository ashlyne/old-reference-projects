package com.weebly.foxgenesis.blackjack;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

/**
 * Deck holds 52 playing cards
 *
 * @author 151steinbergs
 */
public class Deck extends CardHandler {

    private static Deck instance;
    private int x;
    private int y;

    static {
        instance = new Deck();
    }

    /**
     * Create a new deck
     */
    private Deck() {
        System.out.println("Created: " + this.getCards().size() + " cards");
    }

    /**
     * Gets the main deck created. Main deck is created a runtime
     *
     * @return Deck
     */
    public static Deck getMainDeck() {
        return instance;
    }

    /**
     * Draws the deck with a given graphics
     *
     * @param g Graphics to use
     */
    public void draw(Graphics g) {
        g.setColor(new Color(.5f, .5f, .5f, .5f));
        g.fillRect(x + 3, y + 3, CardPicUtils.CARD_WIDTH, CardPicUtils.CARD_HEIGHT);
        for (int i = 0; i < this.getCards().size(); i++) {
            Card a = this.getCards().get(i);
            if (a.isShowing()) {
                a.setShowing(false);
            }
            a.draw(g, x - (i / 2), y - (i / 3));
        }
    }

    /**
     * Sets the location of the deck
     *
     * @param x x location on the screen
     * @param y y location on the screen
     */
    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Called when card is added to the deck
     *
     * @param card Card added
     */
    @Override
    public void onCardAdd(Card card) {
        card.drawShadow(false);
    }

    /**
     * Called when a card is taken from the deck
     *
     * @param card Card taken
     */
    @Override
    public void onCardRelease(Card card) {
        card.drawShadow(true);
    }

    /**
     * Creates a new deck
     *
     * @return Deck
     */
    public static Deck getNewDeck() {
        return new Deck();
    }

    @Override
    public String toString() {
        return "Deck: {X=" + x + "; Y=" + y + "; MainDeck=" + (this == instance) + "}";
    }
}
