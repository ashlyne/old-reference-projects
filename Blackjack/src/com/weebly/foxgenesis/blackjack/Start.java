package com.weebly.foxgenesis.blackjack;

import com.weebly.foxgenesis.debug.ConsoleFrame;
import java.lang.reflect.Field;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

/*
 * Starts the game
 */
public class Start extends AppGameContainer {

    /**
     * Called by the main method to create a new game
     *
     * @throws SlickException
     */
    public Start() throws SlickException {
        super(new BlackJack(), 700, 500, false);
//        GraphicsEnvironment graphics = GraphicsEnvironment.getLocalGraphicsEnvironment();
//        double w = graphics.getMaximumWindowBounds().getWidth();
//        double h = graphics.getMaximumWindowBounds().getHeight();
//        this.setDisplayMode((int)w,(int)h,false);
        this.setTitle("BlackJack");
        this.setIcon("/textures/redchip.png");
        this.setSoundOn(true);
        this.setUpdateOnlyWhenVisible(true);
        this.start();
    }

    public static void main(String[] args) throws SlickException {
        JFrame frame = ConsoleFrame.createConsoleFrame("blackjack");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Used to set path to jar folder
//        System.setProperty("java.library.path", Start.class.getProtectionDomain().getCodeSource().getLocation().getPath());
//
//        Field fieldSysPath;
//        try {
//            fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
//            fieldSysPath.setAccessible(true);
//            fieldSysPath.set(null, null);
//        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e1) {
//        }
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            // If Nimbus is not available, you can set the GUI to another look and feel.
        }
        String name = JOptionPane.showInputDialog(null, "Please Enter Your Name: ", "Name needed", JOptionPane.QUESTION_MESSAGE);
        if(name == null)
            System.exit(0);
        Data.updatePlayer(name);
        AppGameContainer game = new Start();
    }
}
