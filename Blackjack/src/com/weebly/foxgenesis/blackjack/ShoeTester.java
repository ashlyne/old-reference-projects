/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.weebly.foxgenesis.blackjack;

/**
 *
 * @author 151steinbergs
 */
public class ShoeTester {

    public static void main(String[] args) {
        System.out.println("========= SHOE TESTER ==========");
        System.out.println("Creating new shoe with 1 deck(s) ...");
        Shoe shoe = new Shoe(1);
        shoe.shuffle();
        shoe.shuffle();
        System.out.println(shoe);
        System.out.println("setting location to 35,35 ...");
        shoe.setLocation(35, 35);
        System.out.println(shoe);

        System.out.println();

        System.out.println("Creating new shoe with 2 deck(s) ...");
        Shoe shoe2 = new Shoe(2);
        shoe2.shuffle();
        shoe2.shuffle();
        System.out.println(shoe2);
        System.out.println("setting location to 35,35 ...");
        shoe2.setLocation(35, 35);
        System.out.println(shoe2);
        System.out.println("========= SHOE TESTER ==========");
    }
    /*
     * ========= SHOE TESTER ==========
     Creating new shoe with 1 deck(s) ...
     created card[1:S]
     created card[2:S]
     created card[3:S]
     created card[4:S]
     created card[5:S]
     created card[6:S]
     created card[7:S]
     created card[8:S]
     created card[9:S]
     created card[10:S]
     created card[11:S]
     created card[12:S]
     created card[13:S]
     created card[1:D]
     created card[2:D]
     created card[3:D]
     created card[4:D]
     created card[5:D]
     created card[6:D]
     created card[7:D]
     created card[8:D]
     created card[9:D]
     created card[10:D]
     created card[11:D]
     created card[12:D]
     created card[13:D]
     created card[1:C]
     created card[2:C]
     created card[3:C]
     created card[4:C]
     created card[5:C]
     created card[6:C]
     created card[7:C]
     created card[8:C]
     created card[9:C]
     created card[10:C]
     created card[11:C]
     created card[12:C]
     created card[13:C]
     created card[1:H]
     created card[2:H]
     created card[3:H]
     created card[4:H]
     created card[5:H]
     created card[6:H]
     created card[7:H]
     created card[8:H]
     created card[9:H]
     created card[10:H]
     created card[11:H]
     created card[12:H]
     created card[13:H]
     Created: 52 cards
     Mon Feb 24 08:54:47 CST 2014 INFO:Initialising sounds..
     Mon Feb 24 08:54:47 CST 2014 INFO:- Sound works
     Mon Feb 24 08:54:47 CST 2014 INFO:- 64 OpenAL source available
     Mon Feb 24 08:54:47 CST 2014 INFO:- Sounds source generated
     Shoe: {decks=1; X=0; Y=0}
     setting location to 35,35 ...
     Shoe: {decks=1; X=35; Y=35}

     Creating new shoe with 2 deck(s) ...
     created card[1:S]
     created card[2:S]
     created card[3:S]
     created card[4:S]
     created card[5:S]
     created card[6:S]
     created card[7:S]
     created card[8:S]
     created card[9:S]
     created card[10:S]
     created card[11:S]
     created card[12:S]
     created card[13:S]
     created card[1:D]
     created card[2:D]
     created card[3:D]
     created card[4:D]
     created card[5:D]
     created card[6:D]
     created card[7:D]
     created card[8:D]
     created card[9:D]
     created card[10:D]
     created card[11:D]
     created card[12:D]
     created card[13:D]
     created card[1:C]
     created card[2:C]
     created card[3:C]
     created card[4:C]
     created card[5:C]
     created card[6:C]
     created card[7:C]
     created card[8:C]
     created card[9:C]
     created card[10:C]
     created card[11:C]
     created card[12:C]
     created card[13:C]
     created card[1:H]
     created card[2:H]
     created card[3:H]
     created card[4:H]
     created card[5:H]
     created card[6:H]
     created card[7:H]
     created card[8:H]
     created card[9:H]
     created card[10:H]
     created card[11:H]
     created card[12:H]
     created card[13:H]
     Created: 52 cards
     created card[1:S]
     created card[2:S]
     created card[3:S]
     created card[4:S]
     created card[5:S]
     created card[6:S]
     created card[7:S]
     created card[8:S]
     created card[9:S]
     created card[10:S]
     created card[11:S]
     created card[12:S]
     created card[13:S]
     created card[1:D]
     created card[2:D]
     created card[3:D]
     created card[4:D]
     created card[5:D]
     created card[6:D]
     created card[7:D]
     created card[8:D]
     created card[9:D]
     created card[10:D]
     created card[11:D]
     created card[12:D]
     created card[13:D]
     created card[1:C]
     created card[2:C]
     created card[3:C]
     created card[4:C]
     created card[5:C]
     created card[6:C]
     created card[7:C]
     created card[8:C]
     created card[9:C]
     created card[10:C]
     created card[11:C]
     created card[12:C]
     created card[13:C]
     created card[1:H]
     created card[2:H]
     created card[3:H]
     created card[4:H]
     created card[5:H]
     created card[6:H]
     created card[7:H]
     created card[8:H]
     created card[9:H]
     created card[10:H]
     created card[11:H]
     created card[12:H]
     created card[13:H]
     Created: 52 cards
     Shoe: {decks=2; X=0; Y=0}
     setting location to 35,35 ...
     Shoe: {decks=2; X=35; Y=35}
     ========= SHOE TESTER ==========
     */
}
