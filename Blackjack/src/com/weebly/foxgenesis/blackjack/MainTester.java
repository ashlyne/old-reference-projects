package com.weebly.foxgenesis.blackjack;

/**
 * Main Tester tests all classes in the project
 *
 * @author 151steinbergs
 */
public class MainTester {

    public static void main(String[] args) {
        DataTester.main(args);
        System.out.println();

        ContainerTester.main(args);
        System.out.println();

        ChipTester.main(args);
        System.out.println();

        DeckTester.main(args);
        System.out.println();

        ShoeTester.main(args);
        System.out.println();

        CardTester.main(args);
        System.out.println();
        
        HandTester.main(args);
        System.out.println();

        GraphicsTester.main(args);
        System.out.println();
    }
}
