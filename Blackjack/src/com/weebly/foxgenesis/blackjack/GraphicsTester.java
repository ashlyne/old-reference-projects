package com.weebly.foxgenesis.blackjack;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * Graphics Tester to test classes that require a slick instance
 *
 * @author 151steinbergs
 */
public class GraphicsTester extends AppGameContainer {

    /**
     * Called by the main method to create a new game
     *
     * @throws SlickException
     */
    public GraphicsTester() throws SlickException {
        super(new GraphicTest(), 700, 500, false);
        this.setIcon("/textures/redchip.png");
        this.setSoundOn(true);
        this.start();
    }

    public static void main(String[] args) {
        System.out.println("============= GRAPHICS TESTER ===============");
        System.out.println("Creating graphics tester...");
        Data.updatePlayer("tester");
        try {
            new GraphicsTester();
        } catch (SlickException ex) {
        }
    }
}

class GraphicTest implements Game {

    private Button button;

    @Override
    public void init(GameContainer gc) throws SlickException {
        System.out.println("============= BUTTON TESTER ==============");
        button = new Button("Button Tester");
        button.listen(gc.getInput());
        System.out.println(button);
        System.out.println("setting location to 35,35 ...");
        button.setLocation(35, 35);
        System.out.println(button);
        System.out.println("setting enabled to false...");
        button.setEnabled(false);
        System.out.println(button);
        System.out.println("setting enabled to true...");
        button.setEnabled(true);
        System.out.println(button);
        System.out.println("setting button action...");
        button.setAction(new Action() {
            @Override
            public void act(Input input) {
                button.setText("Hehehe that tickles!");
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        button.setText("Button Tester");
                    }
                }, 5000);
            }
        });
        System.out.println("============= BUTTON TESTER ==============");
        System.out.println();

        PlayerTester.main(gc.getInput());
        System.out.println();

        DealerTester.main(gc.getInput());
        System.out.println();

        LeaderBoardTester.main(gc.getInput());
        System.out.println();
    }

    @Override
    public void update(GameContainer gc, int i) throws SlickException {
    }

    @Override
    public void render(GameContainer gc, Graphics grphcs) throws SlickException {
        button.draw(grphcs);
    }

    @Override
    public boolean closeRequested() {
        return true;
    }

    @Override
    public String getTitle() {
        return "Graphics Tester";
    }
    /*
     * ============= GRAPHICS TESTER ===============
     Creating graphics tester...
     Tue Feb 25 10:16:50 CST 2014 INFO:Slick Build #237
     Tue Feb 25 10:16:50 CST 2014 INFO:LWJGL Version: 2.9.1
     Tue Feb 25 10:16:50 CST 2014 INFO:OriginalDisplayMode: 1440 x 900 x 32 @60Hz
     Tue Feb 25 10:16:50 CST 2014 INFO:TargetDisplayMode: 700 x 500 x 0 @0Hz
     Tue Feb 25 10:16:50 CST 2014 INFO:Starting display 700x500
     Tue Feb 25 10:16:50 CST 2014 INFO:Use Java PNG Loader = true
     WARNING: Found unknown Windows version: Windows 7
     Attempting to use default windows plug-in.
     Loading: net.java.games.input.DirectAndRawInputEnvironmentPlugin
     Tue Feb 25 10:16:50 CST 2014 INFO:Found 2 controllers
     Tue Feb 25 10:16:50 CST 2014 INFO:0 : Lenovo USB Keyboard
     Tue Feb 25 10:16:50 CST 2014 INFO:1 : Lenovo USB Keyboard
     */
}
