package com.weebly.foxgenesis.blackjack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Saves data into the text file
 *
 * @author 151steinbergs
 */
public final class Data {

    private static String player = "Default";
    private static File file;
    private static HashMap<String, Double> values;

    static {
        values = new HashMap<>();
        file = new File("data.bjd");

        try {
            if (!Data.file.exists()) {
                Data.file.createNewFile();
            }
        } catch (IOException e) {
        }
        updateHash();
        updateBank(getBank());
    }

    /**
     * Changes current player to another
     *
     * @param p players name
     */
    public static void updatePlayer(String p) {
        if (p.equals("")) {
            player = "Default";
        } else {
            player = p;
        }
        updateHash();
        updateBank(getBank());
    }

    /**
     * Updates the hashmap with new values
     */
    private static void updateHash() {
        try {
            for (String line : getFileText()) {
                String[] temp = line.split(":");
                values.put(temp[0], temp.length > 2 ? (0) : (Double.parseDouble(temp[1])));
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    /**
     * Gets the players name that is currently being used
     *
     * @return String
     */
    public static String getPlayer() {
        return player;
    }

    /**
     * Gets the text file text into a list
     *
     * @return List of strings
     */
    private static List<String> getFileText() {
        List<String> temp = new ArrayList<>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = reader.readLine()) != null) {
                temp.add(line);
            }
        } catch (IOException e) {
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
            }
        }
        return temp;
    }

    /**
     * turns the hashmap into a list
     *
     * @return Lists of strings
     */
    private static List<String> hashToList() {
        List<String> temp = new ArrayList<>();
        String[] keys = values.keySet().toArray(new String[]{});
        Double[] bank = values.values().toArray(new Double[]{});

        if (keys.length != bank.length) {
            throw new ArrayIndexOutOfBoundsException("Keys and Bank size are different!");
        }

        for (int i = 0; i < keys.length; i++) {
            temp.add(keys[i] + ":" + bank[i]);
        }

        return temp;
    }

    /**
     * Updates the players money in the text file
     *
     * @param bank players money
     */
    public static void updateBank(double bank) {
        FileWriter writer = null;
        List<String> temp = hashToList();
        boolean updated = false;
        String toWrite = "";
        for (String a : temp) {
            if (a.contains(player)) {
                toWrite += player + ":" + bank + "\n";
                updated = true;
            } else {
                toWrite += a + "\n";
            }
        }
        if (!updated) {
            toWrite += player + ":" + bank + "\n";
        }
        try {
            writer = new FileWriter(file);
            writer.write(toWrite);
        } catch (IOException e) {
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
            }
        }
        updateHash();
    }

    /**
     * Returns the players current money in the text file
     *
     * @return double
     */
    public static double getBank() {
        updateHash();
        try {
            return values.get(player);
        } catch (NullPointerException e) {
            return 100;
        }
    }

    /**
     * Gets the hashmap of players and their bank
     *
     * @return HashMap
     */
    public static HashMap<String, Double> getHashMap() {
        return values;
    }
}
