package com.weebly.foxgenesis.blackjack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Class to hold cards for a sub class
 *
 * @author 151steinbergs
 */
public abstract class CardHandler {

    private ArrayList<Card> cards;
    private ArrayList<CardAddChecker> addHandlers;

    /**
     * Create a new CardHandler
     */
    public CardHandler() {
        cards = new ArrayList<>();
        addHandlers = new ArrayList<>();
        if (this instanceof Deck) {
            createDeck();
        }
    }

    /**
     * fills the cards with a new deck
     */
    private void createDeck() {
        for (int i = 1; i < 14; i++) {
            cards.add(new Card(i + ":S"));
        }
        for (int i = 1; i < 14; i++) {
            cards.add(new Card(i + ":D"));
        }
        for (int i = 1; i < 14; i++) {
            cards.add(new Card(i + ":C"));
        }
        for (int i = 1; i < 14; i++) {
            cards.add(new Card(i + ":H"));
        }
        for (Card a : cards) {
            a.drawShadow(false);
        }
    }

    /**
     * Moves a card from this CardHandler to another
     *
     * @param card Card to move
     * @param handler Handler to send card to
     */
    public void moveCard(Card card, CardHandler handler) {
        if (cards.contains(card)) {
            if (handler.add(card)) {
                onCardRelease(card);
                cards.remove(card);
                Sounds.CARD_SLAP.playAsSoundEffect(1f, 1f, false);
            }
        }
    }

    /**
     * Adds a new CardAddChecker
     *
     * @param checker CardAddChecker to add
     */
    public void addCardAddChecker(CardAddChecker checker) {
        addHandlers.add(checker);
    }

    /**
     * Checks weather a card can be added. If it can, this will return true and
     * add the card otherwise this will return false
     *
     * @param card
     * @return boolean
     */
    private boolean add(Card card) {
        for (CardAddChecker a : addHandlers) {
            if (!a.canAddCard()) {
                return false;
            }
        }
        onCardAdd(card);
        cards.add(card);
        return true;
    }

    /**
     * Finds a card with the given delimiter
     *
     * @param delim String representation of the card
     * @return Card
     */
    public Card findCard(String delim) {
        delim = convert(delim);
        if (hasCard(delim)) {
            for (Card a : cards) {
                if (a.toString().equalsIgnoreCase(delim)) {
                    return a;
                }
            }
        }
        return null;
    }

    /**
     * Checks to see if a card exists in this handler
     *
     * @param delim String represtation of the Card
     * @return boolean
     */
    public boolean hasCard(String delim) {
        delim = convert(delim);
        for (Card a : cards) {
            if (a.toString().equalsIgnoreCase(delim)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the cards that this handler holds
     *
     * @return List of cards
     */
    protected List<Card> getCards() {
        return cards;
    }

    /**
     * Shuffles the cards contained in this handler
     */
    public void shuffle() {
        Random rnd = new Random();
        ArrayList<Card> tempCards = new ArrayList<>();
        for (Card a : this.cards) {
            tempCards.add(a);
        }
        Card[] k = new Card[tempCards.size()];
        k = tempCards.toArray(k);
        for (int i = tempCards.size() - 1; i > 0; i--) {
            int rand = rnd.nextInt(i + 1);
            Card a = k[rand];
            k[rand] = k[i];
            k[i] = a;
        }
        ArrayList<Card> temp = new ArrayList<>();
        temp.addAll(Arrays.asList(k));
        this.cards = temp;
    }

    /**
     * Converts a normal Card's String representation into an integer based one
     *
     * @param delim String representation of the Card
     * @return String
     */
    private String convert(String delim) {
        String n = "";
        String[] temp = delim.split(":");

        switch (temp[0].toLowerCase()) {
            case "a":
                n += "1:";
                break;
            case "k":
                n += "13:";
                break;
            case "q":
                n += "12:";
                break;
            case "j":
                n += "11:";
                break;
            default:
                n += Integer.parseInt(temp[0]) + ":";
                break;
        }

        String suit = temp[1].toLowerCase();
        n += suit;
        return n;
    }

    /**
     * Called when a card is added into this handler
     *
     * @param card Card added
     */
    public abstract void onCardAdd(Card card);

    /**
     * Called when a card is taken from this handler
     *
     * @param card Card taken
     */
    public abstract void onCardRelease(Card card);
}

interface CardAddChecker {

    /**
     * Checks weather a card can be added to a CardHandler
     *
     * @return boolean
     */
    public boolean canAddCard();
}
