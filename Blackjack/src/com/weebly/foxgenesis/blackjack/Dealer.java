package com.weebly.foxgenesis.blackjack;

import org.newdawn.slick.Input;

/**
 * Dealer (house) plays against all blackjack players
 *
 * @author 151steinbergs
 */
public class Dealer extends Player {

    /**
     * Create a new Dealer with a given input and movehandler
     *
     * @param input Input to use
     * @param handler Handler to handle this dealer
     */
    public Dealer(Input input, MoveHandler handler) {
        super(input, handler);
        super.haveButtons(false);
        super.setHand(new DealerHand());
    }

    /**
     * Tells the dealer to make a move
     */
    public void move() {
        if (canMakeAMove()) {
            this.hit();
        } else {
            this.stand();
        }
    }

    /**
     * Checks and returns if the dealer is able to make another move
     *
     * @return boolean
     */
    public boolean canMakeAMove() {
        this.updateValue();
        if (this.getValue() >= 17) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Shows all cards in the dealers hand
     *
     * @param state
     */
    public void showCards(boolean state) {
        for (Card a : this.hand().getCards()) {
            a.setShowing(state);
        }
    }

    /**
     * Resets the dealer
     */
    @Override
    public void reset() {
        showCards(false);
        super.reset();
        this.setPlayersTurn(false);
    }

    /**
     * String representation of the dealer
     *
     * @return String
     */
    @Override
    public String toString() {
        String text = super.toString().replace("}", "$1");
        text += "; Can Make A Move = " + canMakeAMove();
        text += "; Total Value = " + getTotalValue();
        text += "}";
        return text;
    }

    /**
     * Gets the value of all cards in the hand
     *
     * @return int
     */
    public int getTotalValue() {
        int temp = 0;
        for (Card a : this.hand().getCards()) {
            temp += a.getValue();
        }
        return temp;
    }
}

/**
 * Hand that is modified for the dealer
 *
 * @author 151steinbergs
 */
class DealerHand extends Hand {

    /**
     * Method called when card is added
     *
     * @param card Card added
     */
    @Override
    public void onCardAdd(Card card) {
        card.drawShadow(true);
        if (this.getCards().size() == 1) {
            card.setShowing(false);
        } else {
            card.setShowing(true);
        }
    }
}
