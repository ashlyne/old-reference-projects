package com.weebly.foxgenesis.blackjack;

/**
 *
 * @author 151steinbergs
 */
public class HandTester {

    public static void main(String[] args) {
        System.out.println("============= HAND TESTER ===========");
        Hand hand = new Hand();
        System.out.println(hand);
        System.out.println("setting max cards to 3");
        System.out.println(hand);
        System.out.println("============= HAND TESTER ===========");
    }
    /*
     * ============= HAND TESTER ===========
     Hand{maxCards=-1; canAddCard=true}
     setting max cards to 3
     Hand{maxCards=-1; canAddCard=true}
     ============= HAND TESTER ===========
     */
}
