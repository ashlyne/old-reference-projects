package com.weebly.foxgenesis.blackjack;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.openal.SoundStore;

/**
 * Class that holds multiple decks
 *
 * @author 151steinbergs
 */
public class Shoe extends CardHandler {

    private int numDecks;
    private int x, y;

    /**
     * Create a new Shoe with a given number of decks
     *
     * @param decks
     */
    public Shoe(int decks) {
        numDecks = decks;
        for (int i = 0; i < numDecks; i++) {
            Deck deck = Deck.getNewDeck();
            List<Card> temp = new ArrayList<>();
            for (Card a : deck.getCards()) {
                temp.add(a);
            }
            for (Card a : temp) {
                deck.moveCard(a, Shoe.this);
            }
        }
    }

    /**
     * Shuffles the shoe and plays a sound of shuffling
     */
    @Override
    public void shuffle() {
        super.shuffle();
        Sounds.SHUFFLE.playAsSoundEffect(1f, 1f, false);
        SoundStore.get().poll(0);
    }

    /**
     * Gets the number of decks in the Shoe
     *
     * @return int
     */
    public int getNumberOfDecks() {
        return numDecks;
    }

    /**
     * Method called when a card is added to the shoe
     *
     * @param card Card added
     */
    @Override
    public void onCardAdd(Card card) {
        card.drawShadow(false);
    }

    /**
     * Method called when a card is taken from the shoe
     *
     * @param card Card taken
     */
    @Override
    public void onCardRelease(Card card) {
        card.drawShadow(true);
    }

    /**
     * Draws the shoe with a given graphics
     *
     * @param g Graphics to draw with
     */
    public void draw(Graphics g) {
        g.setColor(new Color(.5f, .5f, .5f, .5f));
        g.fillRect(x + 3, y + 3, CardPicUtils.CARD_WIDTH, CardPicUtils.CARD_HEIGHT);
        for (int i = 0; i < this.getCards().size(); i++) {
            Card a = this.getCards().get(i);
            if (a.isShowing()) {
                a.setShowing(false);
            }
            if (a.doesDrawShadow()) {
                a.drawShadow(false);
            }
            a.draw(g, x - (i / 2), y - (i / 3));
        }
    }

    /**
     * Sets the location of the shoe on the screen
     *
     * @param x x location on the screen
     * @param y y location on the screen
     */
    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Shoe: {decks=" + getNumberOfDecks() + "; X=" + x + "; Y=" + y + "}";
    }
}
