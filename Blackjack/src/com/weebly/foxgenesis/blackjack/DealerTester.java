package com.weebly.foxgenesis.blackjack;

import org.newdawn.slick.Input;

/**
 * Dealer Tester
 *
 * @author 151steinbergs
 */
public class DealerTester {

    public static void main(Input input) {
        System.out.println("============ DEALER TESTER ==============");
        Dealer dealer = new Dealer(input, new MoveHandler() {
            @Override
            public void deal(Player player) {
            }

            @Override
            public void stand(Player player) {
            }

            @Override
            public void hit(Player player) {
            }

            @Override
            public void bust(Player player) {
            }

            @Override
            public void blackjack(Player player) {
            }

            @Override
            public void doubleDown(Player player) {
            }

            @Override
            public void getInsurance(Player player, boolean wants) {
            }
        });
        System.out.println(dealer);
        System.out.println("making dealer show his cards....");
        dealer.showCards(true);
        System.out.println(dealer);
        System.out.println("============ DEALER TESTER ==============");
    }
    /*
     * ============ DEALER TESTER ==============
     Player{Players Turn = true; Cards = 0; Max Cards = -1; In Round = false; Value = 0; Has Ace = false; Money = 100.0; Bet = 1.0$1; Can Make A Move = true; Total Value = 0}
     making dealer show his cards....
     Player{Players Turn = true; Cards = 0; Max Cards = -1; In Round = false; Value = 0; Has Ace = false; Money = 100.0; Bet = 1.0$1; Can Make A Move = true; Total Value = 0}
     ============ DEALER TESTER ==============
     */
}
