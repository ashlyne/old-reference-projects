package com.weebly.foxgenesis.blackjack;

/**
 *
 * @author 151steinbergs
 */
public class ContainerTester {

    public static void main(String[] args) {
        System.out.println("============= CONTAINER TESTER ===========");
        Container con = new Container();
        System.out.println(con);
        System.out.println("Setting location to 35,35...");
        con.setLocation(35, 35);
        System.out.println(con);
        System.out.println("setting size to 356,361...");
        con.setSize(356, 361);
        System.out.println(con);
        System.out.println("Checking if container contains point 36,36...");
        System.out.println("contains=" + con.contains(36, 36));
        System.out.println("============= CONTAINER TESTER ===========");
    }
    /*
     * ============= CONTAINER TESTER ===========
     Container{X=0.0; Y=0.0; Width=0; Height=0}
     Setting location to 35,35...
     Container{X=35.0; Y=35.0; Width=0; Height=0}
     setting size to 356,361...
     Container{X=35.0; Y=35.0; Width=356; Height=361}
     Checking if container contains point 36,36...
     contains=true
     ============= CONTAINER TESTER ===========
     */
}
