package com.weebly.foxgenesis.blackjack;

import org.newdawn.slick.Input;

/**
 *
 * @author 151steinbergs
 */
public class LeaderBoardTester {

    public static void main(Input input) {
        System.out.println("============= LEADERBOARD TESTER ===========");
        LeaderBoard a = new LeaderBoard(input);
        System.out.println(a);
        System.out.println("============= LEADERBOARD TESTER ===========");
    }
    /*
     * ============= LEADERBOARD TESTER ===========
     LeaderBoard{visible=false}
     ============= LEADERBOARD TESTER ===========
     */
}
