/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.weebly.foxgenesis.blackjack;

/**
 *
 * @author 151steinbergs
 */
public class DeckTester {

    public static void main(String[] args) {
        System.out.println("============== DECK TESTER =============");
        System.out.println("testing main deck....");
        Deck deck = Deck.getMainDeck();
        System.out.println(deck);
        System.out.println("setting location to 35,35....");
        deck.setLocation(35, 35);
        System.out.println(deck);

        System.out.println();

        System.out.println("testing new deck");
        Deck deck2 = Deck.getNewDeck();
        System.out.println(deck2);
        System.out.println("setting location to 35,35....");
        deck2.setLocation(35, 35);
        System.out.println(deck2);
        System.out.println("============== DECK TESTER =============");
    }
    /*
     * ============== DECK TESTER =============
     testing main deck....
     Failed to create button picture!
     created card[1:S]
     created card[2:S]
     created card[3:S]
     created card[4:S]
     created card[5:S]
     created card[6:S]
     created card[7:S]
     created card[8:S]
     created card[9:S]
     created card[10:S]
     created card[11:S]
     created card[12:S]
     created card[13:S]
     created card[1:D]
     created card[2:D]
     created card[3:D]
     created card[4:D]
     created card[5:D]
     created card[6:D]
     created card[7:D]
     created card[8:D]
     created card[9:D]
     created card[10:D]
     created card[11:D]
     created card[12:D]
     created card[13:D]
     created card[1:C]
     created card[2:C]
     created card[3:C]
     created card[4:C]
     created card[5:C]
     created card[6:C]
     created card[7:C]
     created card[8:C]
     created card[9:C]
     created card[10:C]
     created card[11:C]
     created card[12:C]
     created card[13:C]
     created card[1:H]
     created card[2:H]
     created card[3:H]
     created card[4:H]
     created card[5:H]
     created card[6:H]
     created card[7:H]
     created card[8:H]
     created card[9:H]
     created card[10:H]
     created card[11:H]
     created card[12:H]
     created card[13:H]
     Created: 52 cards
     Deck: {X=0; Y=0; MainDeck=true}
     setting location to 35,35....
     Deck: {X=35; Y=35; MainDeck=true}

     testing new deck
     created card[1:S]
     created card[2:S]
     created card[3:S]
     created card[4:S]
     created card[5:S]
     created card[6:S]
     created card[7:S]
     created card[8:S]
     created card[9:S]
     created card[10:S]
     created card[11:S]
     created card[12:S]
     created card[13:S]
     created card[1:D]
     created card[2:D]
     created card[3:D]
     created card[4:D]
     created card[5:D]
     created card[6:D]
     created card[7:D]
     created card[8:D]
     created card[9:D]
     created card[10:D]
     created card[11:D]
     created card[12:D]
     created card[13:D]
     created card[1:C]
     created card[2:C]
     created card[3:C]
     created card[4:C]
     created card[5:C]
     created card[6:C]
     created card[7:C]
     created card[8:C]
     created card[9:C]
     created card[10:C]
     created card[11:C]
     created card[12:C]
     created card[13:C]
     created card[1:H]
     created card[2:H]
     created card[3:H]
     created card[4:H]
     created card[5:H]
     created card[6:H]
     created card[7:H]
     created card[8:H]
     created card[9:H]
     created card[10:H]
     created card[11:H]
     created card[12:H]
     created card[13:H]
     Created: 52 cards
     Deck: {X=0; Y=0; MainDeck=false}
     setting location to 35,35....
     Deck: {X=35; Y=35; MainDeck=false}
     ============== DECK TESTER =============
     */
}
