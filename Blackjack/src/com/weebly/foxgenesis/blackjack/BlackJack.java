package com.weebly.foxgenesis.blackjack;

import static com.weebly.foxgenesis.blackjack.Sounds.CARD_SLAP;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

/**
 * Main game class for blackjack that controls the entire game
 *
 * @author 151steinbergs
 */
public class BlackJack implements Game, MoveHandler {

    private Table table;
    private Shoe shoe;
    private Player player;
    private Dealer dealer;
    private CardHandler pile;
    private LinkedList<String> que;
    private LeaderBoard scores;
    private int tick = 0;
    private boolean wait = false;
    private boolean noNewRound = false;
    private boolean askedForInsurance = false;

    /**
     * Called when the frame is asked to close
     *
     * @return boolean
     */
    @Override
    public boolean closeRequested() {
        return true;
    }

    /*
     * Gets the title of the frame
     */
    @Override
    public String getTitle() {
        return "BlackJack";
    }

    /**
     * Creates the games data
     *
     * @param c GameContainer
     * @throws SlickException
     */
    @Override
    public void init(final GameContainer c) throws SlickException {
        try {
            que = new LinkedList<>();
            c.setShowFPS(false);
            c.setTargetFrameRate(60);
            c.setMouseCursor(CardPicUtils.cursor, 0, 0);

            table = new Table();

            player = new Player(c.getInput(), this);
            player.setLocation(40, (c.getHeight() - player.getHeight()) - 130);

            dealer = new Dealer(c.getInput(), this);
            dealer.setLocation(40, 46);
            dealer.setPlayersTurn(false);

            pile = new Pile();

            shoe = new Shoe(1);
            shoe.setLocation(400, 211);
            shoe.shuffle();
            shoe.shuffle();

            scores = new LeaderBoard(c.getInput());
            scores.setSize(c.getWidth() - 40, c.getHeight());
            scores.setLocation(c.getWidth() - (scores.getWidth() / 4), 0);
            if(Data.getPlayer().equalsIgnoreCase("Rick"))
                Sounds.RICK.playAsMusic(.8f, .8f, true);
            else
                Sounds.BACKGROUND.playAsMusic(.8f, .8f, true);
        } catch (HeadlessException | SlickException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            System.exit(1);
        }
    }

    /**
     * Draws the game
     *
     * @param c GameContainer to draw on
     * @param g Graphics to draw with
     * @throws SlickException
     */
    @Override
    public void render(GameContainer c, Graphics g) throws SlickException {
        table.draw(c, g);
        shoe.draw(g);
        player.draw(g);
        dealer.draw(g);
        scores.draw(g);

        if (tick < 10) {
            tick++;
        } else if (tick == 10) {
            tick(c, g);
        }
    }

    /**
     * Tick method to display and draw game data outside of the render and
     * update methods
     *
     * @param container GameContainer
     * @param g Graphics
     */
    private void tick(GameContainer container, Graphics g) {
        if (player.hand().getCards().size() == 2 && !askedForInsurance) {
            askedForInsurance = true;
            checkInsurance();
        }
        if (!que.isEmpty()) {
            String msg = que.get(0);
            g.setColor(Color.white);
            drawCenteredString(msg, container, g);
            if (msg.equalsIgnoreCase("You Win!")) {
                Sounds.WIN.playAsSoundEffect(1f, 1f, false);
            } else if (msg.equalsIgnoreCase("House Wins!")) {
                Sounds.LOSE.playAsSoundEffect(1f, 1f, false);
            }
            wait = true;
            que.remove(0);
        }

    }

    /**
     * Called when the game wants to update
     *
     * @param c GameContainer to update
     * @param delta time passed
     * @throws SlickException
     */
    @Override
    public void update(GameContainer c, int delta) throws SlickException {
        try {
            if (!wait) {
                if (dealer.isPlayersTurn()) {
                    dealer.showCards(true);
                    dealer.move();
                    Thread.sleep(1000);
                }
                Thread.sleep(20);
            } else {
                Thread.sleep(2500);
                wait = false;
                if (!noNewRound) {
                    newRound();
                } else {
                    noNewRound = false;
                }
            }
        } catch (InterruptedException e) {
        }
    }

    /**
     * Draws a string in the center of the GameContainer
     *
     * @param s String to draw
     * @param c GameContainer to draw on
     * @param g Graphics to draw with
     */
    private void drawCenteredString(String s, GameContainer c, Graphics g) {
        int height = g.getFont().getHeight(s);
        int width = g.getFont().getWidth(s);
        int x = (c.getWidth() - width) / 2;
        int y = (c.getHeight() - height) / 2;
        g.setColor(new Color(0f, 0f, 0f, .8f));
        g.fillRect(x, y, width + 5, height + 5);
        g.setColor(Color.white);
        g.drawString(s, x, y);
    }

    /**
     * Checks if the deck needs to be shuffled (note): this only checks if there
     * are at least 2 cards left
     */
    private void checkForShuffle() {
        checkForShuffle(2);
    }

    /**
     * Checks if the deck needs to be shuffled with a given amount of cards
     *
     * @param amount amount needed to not shuffle
     */
    private void checkForShuffle(int amount) {
        if (shoe.getCards().size() < amount) {
            que.add("Shuffling");
            noNewRound = true;
            List<Card> temp = new ArrayList<>();
            for (Card a : pile.getCards()) {
                temp.add(a);
            }
            for (Card a : temp) {
                pile.moveCard(a, shoe);
            }
            shoe.shuffle();
            shoe.shuffle();
        }
    }

    /**
     * Called when a player asked to be dealt cards
     *
     * @param player player that calls the deal
     */
    @Override
    public void deal(Player player) {
        removeCards();
        System.out.println("deal");
        checkForShuffle();
        player.dealCards(shoe);
        checkForShuffle();
        dealer.dealCards(shoe);
    }

    /**
     * Called when a player wants to stand
     *
     * @param player Player to stand
     */
    @Override
    public void stand(Player player) {
        if (player == this.player) {
            player.setPlayersTurn(false);
            dealer.setPlayersTurn(true);
            dealer.showCards(true);
        } else if (player == dealer) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            if (dealer.getValue() > this.player.getValue()) {
                Data.updateBank(this.player.getMoney() - this.player.getBet());
                que.add("House Wins!");
            } else if (this.player.getValue() > dealer.getValue()) {
                Data.updateBank(this.player.getMoney() + this.player.getBet());
                que.add("You Win!");
            } else {
                que.add("PUSH!");
            }
        } else {
            System.err.println("Failed to get player in stand");
        }
    }

    /**
     * Called when a player wants to be hit
     *
     * @param player sPlayer to be hit
     */
    @Override
    public void hit(Player player) {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
        }
        checkForShuffle(1);
        shoe.moveCard(shoe.getCards().get(0), player.hand());
        player.updateValue();
        if (!(player instanceof Dealer)) {
            if (player.hand().getCards().size() == 5 && player.getValue() <= 21) {
                blackjack(this.player);
            }
        }
    }

    /**
     * Called when a player goes over 21
     *
     * @param player Player that went over
     */
    @Override
    public void bust(Player player) {
        if (player == this.player) {
            Data.updateBank(this.player.getMoney() - this.player.getBet());
            que.add("House Wins!");
        } else {
            Data.updateBank(this.player.getMoney() + this.player.getBet());
            que.add("You Win!");
        }
    }

    /**
     * Called when a player is dealt blackjack
     *
     * @param player Player that got blackjack
     */
    @Override
    public void blackjack(Player player) {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
        }
        if (player == this.player) {
            Data.updateBank(this.player.getMoney() + this.player.getBet());
            que.add("You Win!");
        } else {
            dealer.showCards(true);
            Data.updateBank(this.player.getMoney() - this.player.getBet());
            que.add("House Wins!");
        }
    }

    /**
     * Called when a player wants to double down
     *
     * @param player Player to double down
     */
    @Override
    public void doubleDown(Player player) {
        player.setBet(player.getBet() * 2);
        player.hit();
        if (player.getValue() <= 21) {
            stand(player);
        }
    }

    /**
     * Creates a new round
     */
    public void newRound() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
        }
        askedForInsurance = false;
        removeCards();
        player.reset();
        dealer.reset();
        player.setPlayersTurn(true);
        player.updateValue();
        dealer.updateValue();
    }

    /**
     * Resets all aces to their default value
     *
     * @param cards Cards to reset all aces for
     */
    private void resetAces(List<Card> cards) {
        for (Card a : cards) {
            if (a.getRank() == 1) {
                a.setValue(1);
            }
        }
    }

    /**
     * Takes all cards from the dealer and the player and moves them to the
     * discard pile
     */
    private void removeCards() {
        List<Card> temp = new ArrayList<>();
        List<Card> temp2 = new ArrayList<>();
        resetAces(temp);
        resetAces(temp2);
        for (Card a : player.hand().getCards()) {
            temp.add(a);
        }
        for (Card a : dealer.hand().getCards()) {
            temp2.add(a);
        }
        for (Card a : temp) {
            player.hand().moveCard(a, pile);
        }
        for (Card a : temp2) {
            dealer.hand().moveCard(a, pile);
        }
    }

    /**
     * Checks if the dealer has an ace and then asks the player if they want to
     * get insurance or not
     */
    private void checkInsurance() {
        if (dealer.getValue() == 11) {
            int option = JOptionPane.showConfirmDialog(null, "Do you want to buy insurance?", "Dealer has an Ace showing!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            getInsurance(player, option == JOptionPane.YES_OPTION);
        }
    }

    /**
     * Requests the player to choose if they want to buy insurance
     *
     * @param player Player that wants insurance
     * @param wants do they want to buy it or not
     */
    @Override
    public void getInsurance(Player player, boolean wants) {
        double insuranceCost = player.getBet() / 2;
        int dealerValue = this.dealer.getTotalValue();
        if (dealerValue == 21) {
            if (wants) {
                que.add("Your insurance paid off!");
            } else {
                Data.updateBank(player.getMoney() - insuranceCost);
                blackjack(dealer);
            }
        } else if (dealerValue != 21) {
            if (wants) {
                Data.updateBank(player.getMoney() - insuranceCost);
                que.add("You lost your insurance!");
                noNewRound = true;
            }
        }

    }
}

/**
 * Class that holds all game sounds
 *
 * @author 151steinbergs
 */
final class Sounds {

    protected static Audio CARD_SLAP;
    protected static Audio LOSE;
    protected static Audio SHUFFLE;
    protected static Audio WIN;
    protected static Audio BACKGROUND;
    protected static Audio RICK;

    static {
        try {
            CARD_SLAP = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("/sounds/cardSlap.wav"));
            LOSE = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("/sounds/lose.wav"));
            SHUFFLE = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("/sounds/lose.wav"));
            SHUFFLE = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("/sounds/shuffle.wav"));
            WIN = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("/sounds/win.wav"));
            BACKGROUND = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("/sounds/background.wav"));
            RICK = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("/sounds/rick.wav"));
        } catch (IOException e) {
        }
    }
}

/**
 * Class to hold cards
 *
 * @author 151steinbergs
 */
class Pile extends CardHandler {

    /**
     * Method called when a card is added
     *
     * @param card Card added
     */
    @Override
    public void onCardAdd(Card card) {
    }

    /**
     * Method called when a card is taken
     *
     * @param card Card taken
     */
    @Override
    public void onCardRelease(Card card) {
    }
}

/**
 * Class that represents the table
 *
 * @author 151steinbergs
 */
class Table {

    private Image background;

    /**
     * Creates a new table
     */
    public Table() {
        try {
            background = new Image("/textures/background.jpg");
        } catch (SlickException e) {
        }
    }

    /**
     * Draws the Table on a given GameContainer with a given Graphics
     *
     * @param c GameContainer to use
     * @param g Graphics to draw with
     */
    public void draw(GameContainer c, Graphics g) {
        g.drawImage(background, 0, 0, c.getWidth(), c.getHeight(), 0, 0, background.getWidth(), background.getHeight());

    }
}
class RickRoll
{
    protected static Image[] rick;
    protected static int[] time;
    static{
        rick = new Image[27];
        time = new int[27];
        for(int i=0; i<RickRoll.rick.length; i++)
        {
            try {
                rick[i] = new Image("textures/rickroll/frame" + (i+1)+".jpg");
                time[i] = 100;
            } catch (SlickException ex) {
                ex.printStackTrace();
            }
        }
    }
}
