package com.weebly.foxgenesis.blackjack;

/**
 * Class used when subclass needs to hold a location
 *
 * @author 151steinbergs
 */
public class Container {

    private double x;
    private double y;
    private int width;
    private int height;

    /**
     * Sets the location of the container
     *
     * @param x x location on the screen
     * @param y y location on the screen
     */
    public void setLocation(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns the x location on the screen
     *
     * @return double
     */
    public double getX() {
        return x;
    }

    /**
     * Returns the y location on the screen
     *
     * @return double
     */
    public double getY() {
        return y;
    }

    /**
     * Checks to see if a point is inside the container
     *
     * @param x x location on the screen
     * @param y y location on the screen
     * @return boolean
     */
    public boolean contains(int x, int y) {
        if (x >= this.x && x <= this.x + width) {
            if (y >= this.y && y <= this.y + height) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets the size of the container
     *
     * @param width width of the container
     * @param height height of the container
     */
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Gets the width of the container
     *
     * @return int
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the height of the container
     *
     * @return int
     */
    public int getHeight() {
        return height;
    }

    /**
     * Gets a string representation of the container
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Container{X=" + x + "; Y=" + y + "; Width=" + width + "; Height=" + height + "}";
    }
}
