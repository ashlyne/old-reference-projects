package com.weebly.foxgenesis.blackjack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

/**
 * Component that displays the players with the highest money
 *
 * @author 151steinbergs
 */
public class LeaderBoard extends Container {

    private boolean visible = false;
    private Button show;
    private Button exit;

    /**
     * Create a new leaderboard component with the given input
     *
     * @param input
     */
    public LeaderBoard(Input input) {
        show = new Button("LeaderBoards");
        exit = new Button("X");

        exit.listen(input);
        show.listen(input);

        show.setAction(new Action() {
            @Override
            public void act(Input input) {
                show.setVisible(false);
                show.setEnabled(false);
                exit.setVisible(true);
                exit.setEnabled(true);
                visible = true;
            }
        });

        exit.setAction(new Action() {
            @Override
            public void act(Input input) {
                exit.setVisible(false);
                exit.setEnabled(false);
                show.setVisible(true);
                show.setEnabled(true);
                visible = false;
            }
        });

        exit.setVisible(false);
        exit.setEnabled(false);
    }

    /**
     * Returns if the leaderboard is visible or not
     *
     * @return boolean
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Updates the button locations
     */
    private void updateLocations() {
        show.setLocation((int) getX() - show.getWidth(), 0);
        exit.setLocation((int) getX(), 0);
    }

    /**
     * Draws the leaderboard with a given graphics
     *
     * @param g
     */
    public void draw(Graphics g) {
        updateLocations();
        if (visible) {
            g.setColor(Color.white);
            g.fillRect((int) this.getX(), (int) this.getY(), this.getWidth(), this.getHeight());
            g.setColor(Color.black);
            g.drawRect((int) this.getX(), (int) this.getY(), this.getWidth(), this.getHeight());
            drawCenteredString("Leader Board", 15, g);
            exit.draw(g);
            HashMap<String, Double> hash = Data.getHashMap();
            hash = sort(hash);
            for (int i = 0; i < hash.size(); i++) {
                String name = (String) hash.keySet().toArray()[i];
                drawPlayer(40 * (i + 1), name, hash.get(name), g);
            }
        } else {
            show.draw(g);
        }
    }

    /**
     * Draws a string in the center of the leaderboard
     *
     * @param s String to draw
     * @param yOffset y offset of the text
     * @param g Graphics to draw with
     */
    private void drawCenteredString(String s, int yOffset, Graphics g) {
        int height = g.getFont().getHeight(s);
        int width = g.getFont().getWidth(s);
        int x = ((int) getX() + getWidth() - width / 2) / 2;
        g.drawString(s, x, yOffset);
        g.drawLine((int) getX(), yOffset + height + 5, (int) getX() + getWidth(), yOffset + height + 5);
    }

    /**
     * Draws the player at a given y
     *
     * @param y y location to draw
     * @param name Name of the player
     * @param bank player's bank
     * @param g Graphics to draw with
     */
    private void drawPlayer(int y, String name, double bank, Graphics g) {
        g.setColor(Color.black);
        drawCenteredString(name + ": $" + bank, y, g);
    }

    /**
     * Sorts a HashMap by its bank
     *
     * @param passedMap HashMap to sort
     * @return LinkedHashMap
     */
    private LinkedHashMap<String, Double> sort(HashMap<String, Double> passedMap) {
        List<String> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Double> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);
        Collections.reverse(mapValues);
        Collections.reverse(mapKeys);
        LinkedHashMap<String, Double> sortedMap = new LinkedHashMap<>();

        Iterator<Double> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Object val = valueIt.next();
            Iterator<String> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Object key = keyIt.next();
                String comp1 = passedMap.get(key).toString();
                String comp2 = val.toString();

                if (comp1.equals(comp2)) {
                    passedMap.remove(key);
                    mapKeys.remove(key);
                    sortedMap.put((String) key, (Double) val);
                    break;
                }

            }

        }
        return sortedMap;
    }
    
    @Override
    public String toString()
    {
        return "LeaderBoard{visible=" + visible + "}";
    }
}
