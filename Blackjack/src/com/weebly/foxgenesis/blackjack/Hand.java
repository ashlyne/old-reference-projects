package com.weebly.foxgenesis.blackjack;

import org.newdawn.slick.Graphics;

/**
 * Class to hold cards
 *
 * @author 151steinbergs
 */
public class Hand extends CardHandler implements CardAddChecker {

    private Container container;
    private int maxCards = -1;
    private boolean faceup = true;

    /**
     * Creates a new hand that holds cards
     */
    public Hand() {
        container = new Container();
        this.addCardAddChecker(Hand.this);
    }

    /**
     * Method called when card is added
     *
     * @param card Card added
     */
    @Override
    public void onCardAdd(Card card) {
        card.drawShadow(true);
        if (faceup) {
            if (!card.isShowing()) {
                card.setShowing(true);
            }
        }
    }

    /**
     * Sets the max cards that the hand can hold
     *
     * @param amount
     */
    public void setMaxCards(int amount) {
        maxCards = amount;
    }

    @Override
    public void onCardRelease(Card card) {
        card.drawShadow(false);
        card.setShowing(false);
    }

    /**
     * Gets the max cards that the hand can hold
     *
     * @return int
     */
    public int getMaxCards() {
        return maxCards;
    }

    /**
     * draws the hand with a given graphics
     *
     * @param g Graphics to use
     */
    public void draw(Graphics g) {
        int cards = this.getCards().size();
        int temp = (int) container.getX();
        for (int i = 0; i < cards; i++) {
            temp += CardPicUtils.CARD_WIDTH + 10;
            Card card = this.getCards().get(i);
            card.setLocation(temp, container.getY());
            card.draw(g);
        }
    }

    /**
     * Gets the container that the hand uses
     *
     * @return Container
     */
    public Container getContainer() {
        return container;
    }

    /**
     * Checks if the hand can hold another card
     *
     * @return boolean
     */
    @Override
    public boolean canAddCard() {
        if (maxCards == -1) {
            return true;
        } else if (this.getCards().size() + 1 > maxCards) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Gets a string representation of the hand
     * @return String
     */
    @Override
    public String toString()
    {
        return "Hand{maxCards=" + maxCards + "; canAddCard=" + canAddCard() + "}";
    }
}
