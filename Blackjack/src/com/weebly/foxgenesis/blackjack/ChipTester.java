package com.weebly.foxgenesis.blackjack;

/**
 * Class to test the chips
 *
 * @author 151steinbergs
 */
public class ChipTester {

    public static void main(String[] args) {
        System.out.println("========= CHIP TESTER ========");
        for (Chips a : Chips.values()) {
            System.out.println("Chip[" + a.name() + "]: {value=" + a.getValue() + "}");
        }
        System.out.println("========= CHIP TESTER ========");
    }
    /*
     * ========= CHIP TESTER ========
     Chip[RED]: {value=100}
     Chip[BLUE]: {value=10}
     Chip[PURPLE]: {value=1000}
     Chip[GOLD]: {value=10000}
     Chip[GREEN]: {value=1}
     ========= CHIP TESTER ========
     */
}
