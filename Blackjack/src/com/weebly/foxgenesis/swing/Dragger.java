package com.weebly.foxgenesis.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Dragger extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = -5311348530697062828L;
    private int type = 0;
    public static int VERTICAL = 1;
    public static int HORIZONTAL = 0;
    private JFrame component;
    private int posX = 0, posY = 0;
    private BufferedImage background;
    private boolean drawTitle = true;

    public Dragger(JFrame component) {
        this.component = component;
        setUp();
    }

    public Dragger(JFrame component, int type) {
        this.type = type;
        this.component = component;
        setUp();
    }

    private void setUp() {
        posX = this.getX();
        posY = this.getY();
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                posX = e.getX();
                posY = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent evt) {
                component.setLocation((evt.getXOnScreen() - getX()) - posX, (evt.getYOnScreen() - getY()) - posY);

            }
        });
    }

    @Override
    public void setSize(int width, int height) {
        Dimension dim = new Dimension(width, height);
        super.setSize(width, height);
        super.setMaximumSize(dim);
        super.setMinimumSize(dim);
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(this.getBackground());
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        if (background != null) {
            g.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), this.getForeground(), this);
        }
        if (drawTitle) {
            g.setColor(Color.white);
            g.setFont(new Font("Arial", Font.BOLD, 20));
            g.drawString(component.getTitle(), 10, this.getHeight() - 10);
        }
    }

    public void drawTitle(boolean state) {
        this.drawTitle = state;
        repaint();
    }

    public int getType() {
        return type;
    }

    @Override
    public JFrame getParent() {
        return component;
    }

    public void setImage(BufferedImage image) {
        this.background = image;
    }
}