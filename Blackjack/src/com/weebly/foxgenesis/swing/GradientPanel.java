package com.weebly.foxgenesis.swing;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class GradientPanel extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = -6398330266626021043L;
    private static final int N = 32;
    private Color color1;
    private Color color2;

    public GradientPanel() {
        this.setBorder(BorderFactory.createEmptyBorder(N, N, N, N));
    }

    public GradientPanel setColor(Color color1, Color color2) {
        this.color1 = color1;
        this.color2 = color2;
        return this;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        if (color1 == null) {
            color1 = getBackground();
        }
        if (color2 == null) {
            color2 = color1.darker();
        }
        int w = getWidth();
        int h = getHeight();
        GradientPaint gp = new GradientPaint(
                0, 0, color1, 0, h, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, w, h);
    }
}