/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.weebly.foxgenesis.debug;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.PrintStream;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author 151steinbergs
 */
public class ConsolePanel extends JPanel {

    public ConsolePanel() {
        super();
        this.setLayout(new BorderLayout());
        this.setBackground(Color.white);

        JTextArea panel = new JTextArea();
        panel.setWrapStyleWord(true);
        panel.setEditable(false);
        panel.setBackground(Color.white);
        TextAreaOutputStream out = new TextAreaOutputStream(panel, "Console");
        System.setOut(new PrintStream(out));
        this.add(panel, BorderLayout.CENTER);
    }
}
