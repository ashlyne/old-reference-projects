/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.weebly.foxgenesis.debug;

import com.weebly.foxgenesis.swing.Dragger;
import com.weebly.foxgenesis.swing.GradientPanel;
import com.weebly.foxgenesis.swing.RUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author 151steinbergs
 */
public class ConsoleFrame extends JFrame {

    private static HashMap<String, JFrame> consoles;

    static {
        consoles = new HashMap<>();
    }

    public static JFrame createConsoleFrame(String title) {
        ConsoleFrame frame = new ConsoleFrame("Console: " + title);
        JScrollPane scroll = new JScrollPane(new ConsolePanel());
        frame.add(scroll, BorderLayout.CENTER);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ConsoleFrame.consoles.put(title, frame);
        return frame;
    }

    public static JFrame getConsoleByTitle(String title) {
        return consoles.get(title);
    }

    public static HashMap<String, JFrame> getConsoles() {
        return consoles;
    }
    private boolean full = false;

    private ConsoleFrame(String title) {
        super(title);
        this.setSize(500, 500);
        this.setState(JFrame.NORMAL);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setUndecorated(true);

        GraphicsEnvironment g = GraphicsEnvironment.getLocalGraphicsEnvironment();
        this.setLocation((int) (g.getCenterPoint().getX() / 1.5), (int) g.getCenterPoint().getY() / 2);

        GradientPanel content = new GradientPanel();
        this.setContentPane(content);
        content.setLayout(new BorderLayout());
        content.setBorder(BorderFactory.createEtchedBorder(0));

        JPanel top = new JPanel();
        top.setBackground(this.getBackground());
        top.setLayout(new BorderLayout());
        content.add(top, BorderLayout.PAGE_START);

        Dragger dragger = new Dragger(this, Dragger.HORIZONTAL);
        dragger.setBorder(BorderFactory.createEtchedBorder());
        dragger.setBackground(Color.DARK_GRAY);
        dragger.setLayout(new BorderLayout());
        dragger.setImage(null);
        top.add(dragger, BorderLayout.CENTER);

        JPanel end = new JPanel();
        end.setBackground(null);
        end.setOpaque(false);
        dragger.add(end, BorderLayout.LINE_END);
        end.add(minimizeButton(), BorderLayout.LINE_END);
        end.add(resizeButton(g), BorderLayout.LINE_END);
        end.add(closeButton(), BorderLayout.LINE_END);
    }

    private JButton closeButton() {
        final JButton button = new JButton("  X  ");
        button.setFont(new Font("Arial", Font.BOLD, 19));
        button.setUI(new RUI());
        button.setBorder(null);
        button.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                if (getDefaultCloseOperation() == JFrame.EXIT_ON_CLOSE) {
                    System.exit(1);
                } else if (getDefaultCloseOperation() == JFrame.DISPOSE_ON_CLOSE) {
                    dispose();
                }
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                button.setBackground(Color.RED);
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                button.setBackground(Color.RED.darker());
            }

            @Override
            public void mousePressed(MouseEvent arg0) {
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
            }
        });
        button.setBackground(Color.RED.darker());
        button.setForeground(Color.WHITE);
        return button;
    }

    private JButton minimizeButton() {
        final JButton button = new JButton("  --  ");
        button.setFont(new Font("Arial", Font.BOLD, 19));
        button.setUI(new RUI());
        button.setBackground(Color.DARK_GRAY);
        button.setForeground(Color.WHITE);
        button.setBorder(null);
        button.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                setState(JFrame.ICONIFIED);
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                button.setBackground(button.getBackground().brighter());
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                button.setBackground(button.getBackground().darker());
            }

            @Override
            public void mousePressed(MouseEvent arg0) {
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
            }
        });
        return button;
    }

    private JButton resizeButton(final GraphicsEnvironment g) {
        final JButton button = new JButton("  ■  ");
        button.setFont(new Font("Arial", Font.BOLD, 19));
        button.setUI(new RUI());
        button.setBackground(Color.DARK_GRAY);
        button.setForeground(Color.WHITE);
        button.setBorder(null);
        button.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                if (full) {
                    full = false;
                    setSize(500, 500);
                    setLocation((int) (g.getCenterPoint().getX() / 1.5), (int) g.getCenterPoint().getY() / 2);
                } else {
                    full = true;
                    int height = (int) g.getMaximumWindowBounds().getHeight();
                    int width = (int) g.getMaximumWindowBounds().getWidth();
                    setSize(width, height);
                    setLocation(0, 0);
                }
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                button.setBackground(button.getBackground().brighter());
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                button.setBackground(button.getBackground().darker());
            }

            @Override
            public void mousePressed(MouseEvent arg0) {
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
            }
        });
        return button;
    }
}
