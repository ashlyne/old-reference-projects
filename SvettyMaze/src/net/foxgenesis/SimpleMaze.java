package net.foxgenesis;

import java.awt.AWTException;
import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import net.foxgenesis.look.Look;
import net.foxgenesis.objects.BouncingHead;
import net.foxgenesis.objects.Goal;
import net.foxgenesis.objects.Player;
import net.foxgenesis.objects.Wall;
import com.gej.core.GWindow;
import com.gej.core.Game;
import com.gej.core.GameState;
import com.gej.core.Global;
import com.gej.graphics.Background;
import com.gej.graphics.GFontAdvanced;
import com.gej.graphics.GFontAnimated;
import static com.gej.input.GInput.Behaviour.INITIAL_DETECT;
import com.gej.input.GKeyBoard;
import com.gej.map.MapLoader;
import com.gej.map.MapManager;
import com.gej.map.MapView;
import com.gej.object.GObject;
import com.gej.object.Tile;
import com.gej.util.ImageTool;

/**
 * Main game class Can be run as an applet or application
 * 
 * @author Seth
 * 
 */
public class SimpleMaze extends Game implements MapLoader {
	private static final long serialVersionUID = 8312540452570703042L;

	// objects
	public static Image PLAYER;
	public static Image WALL;
	public static Image GOAL;
	public static Image SVETTY;
	public static Image panelBackground;
	private BouncingHead[] heads;
	// settings
	public static int tileSize = 32;
	public static int width = 32;
	public static int height = 32;
	public static boolean applet = true;
	public static boolean svetty = false;
	public static boolean collide = false;
	public static boolean motion = true;
	public static boolean camhud = true;
	public static int sens = 60;

	// stats
	public static int level = 0;
	public static long start = 0;

	// misc
	public static SimpleMaze instance;
	private GFontAnimated gameFont = null;
	public static Clip background = null, win = null, loss = null;
	public static boolean focus = false;

	private static BufferedImage img;

	// ============================================================================================================
	/**
	 * Called by the java runtime
	 * 
	 * @param args
	 *            - arguments provided by runtime
	 */
	public static void main(String[] args) {
		Look.loadSyntheticaOxide();
		GWindow window = GWindow.setup(new SimpleMaze());
		window.addWindowListener(new WindowAdapter() {

			@Override
			public void windowActivated(WindowEvent arg0) {
				focus = true;
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				focus = false;
			}

		});
	}

	/**
	 * Creates a new SimpleMaze to be run as a non applet
	 */
	public SimpleMaze() {
		super();
		setState(GameState.GAME_LOADING);
		applet = false;
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				loadGame();
			}
		});
	}

	/**
	 * Called by the applet
	 */
	@Override
	public void init() {
		super.init();
		JOptionPane.showMessageDialog(this,
				"Sorry, applet version is not ready yet");
		System.exit(100);
		// setState(GameState.GAME_LOADING);
		// applet = true;
		// SwingUtilities.invokeLater(new Runnable(){@Override public void run()
		// {loadGame();}});
	}

	/**
	 * Loads the game
	 */
	private void loadGame() {
		System.out.println("loading game.");
		instance = this;
		Stats.updateMain();
		Global.WEB_MODE = applet;
		Global.FRAMES_PER_SECOND = 60;
		Global.TITLE = svetty ? ("Svetty Maze") : ("Photo Maze Runner");

		createMainMenu();
		loadTextures();
		try {
			Background.setBackground(ImageIO.read(
					ClassLoader.getSystemResource("background.jpg"))
					.getScaledInstance(Global.WIDTH, Global.HEIGHT,
							Image.SCALE_SMOOTH));
			loadSounds();
		} catch (Exception e) {
			e.printStackTrace();
		}

		GKeyBoard.setDetectionType(KeyEvent.VK_SPACE, INITIAL_DETECT);
		GKeyBoard.setDetectionType(KeyEvent.VK_ESCAPE, INITIAL_DETECT);

		MapManager.addMap(new MazeMap(instance));
		MapManager.startFirstMap();

		setState(GameState.GAME_INTRO);
		setVisible(true);
		this.getContentPane().requestFocus();
		System.out.println("game started");
		background.loop(Clip.LOOP_CONTINUOUSLY);
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				Speak.sayWittyComment();
			}
		}, 20000, 20000);
	}

	/**
	 * Loads game sounds
	 * 
	 * @throws LineUnavailableException
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	private void loadSounds() throws LineUnavailableException, IOException,
			UnsupportedAudioFileException {
		background = getNewClip(AudioSystem.getAudioInputStream(ClassLoader
				.getSystemResource("background.wav")));
		FloatControl volume = (FloatControl) background
				.getControl(FloatControl.Type.MASTER_GAIN);
		volume.setValue(-27f);
		win = getNewClip(AudioSystem.getAudioInputStream(ClassLoader
				.getSystemResource("win.wav")));
		loss = getNewClip(AudioSystem.getAudioInputStream(ClassLoader
				.getSystemResource("loss.wav")));
	}

	/**
	 * Create a new audio clip
	 * 
	 * @param stream
	 *            - AudioInputStream to use
	 * @return Clip of the audio
	 * @throws LineUnavailableException
	 * @throws IOException
	 */
	private Clip getNewClip(AudioInputStream stream)
			throws LineUnavailableException, IOException {
		final Clip clip = AudioSystem.getClip();
		clip.addLineListener(new LineListener() {
			@Override
			public void update(LineEvent event) {
				if (event.getType() == LineEvent.Type.STOP)
					clip.setFramePosition(0);
			}
		});
		clip.open(stream);
		return clip;
	}

	/**
	 * Sets up the menu panel
	 */
	private void createMainMenu() {
		setGlassPane(new JPanel());
		final JPanel panel = (JPanel) getGlassPane();
		panel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					getGlassPane().setVisible(!getGlassPane().isVisible());
				}
			}
		});
		panel.setLayout(new BorderLayout());
		JPanel bottom = new JPanel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 6361018241226059774L;

			@Override
			public void paintComponent(Graphics g) {
				g.drawImage(SimpleMaze.panelBackground, 0, 0, this.getWidth(),
						this.getHeight(), this);
				super.paintComponent(g);
			}
		};
		JButton quit = new JButton("Quit");
		quit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		quit.setForeground(Color.RED.darker());
		quit.setBackground(Color.white);
		panel.add(new SettingsPane(new Runnable() {
			@Override
			public void run() {
				loadTextures();
			}
		}), BorderLayout.PAGE_END);
		Color green = Color.green.darker().darker();
		bottom.setBackground(new Color(green.getRed(), green.getGreen(), green
				.getBlue(), 0));
		bottom.setBorder(BorderFactory.createEtchedBorder(1));
		bottom.add(quit);
		panel.add(bottom, BorderLayout.PAGE_START);
	}

	/**
	 * Loads the game textures
	 */
	private void loadTextures() {
		if (!applet) {
			gameFont = new GFontAnimated();
			gameFont.addFont(GFontAdvanced.getFont(
					loadImage("ImageFonts/font_green.png"),
					"ImageFonts/DefFontDescriptor.txt"), 500);
			gameFont.addFont(GFontAdvanced.getFont(
					loadImage("ImageFonts/font_cyan.png"),
					"ImageFonts/DefFontDescriptor.txt"), 500);
			gameFont.addFont(GFontAdvanced.getFont(
					loadImage("ImageFonts/font_red.png"),
					"ImageFonts/DefFontDescriptor.txt"), 500);
			gameFont.addFont(GFontAdvanced.getFont(
					loadImage("ImageFonts/font_blue.png"),
					"ImageFonts/DefFontDescriptor.txt"), 500);
			try {
				WALL = ImageIO.read(ClassLoader.getSystemResource("wall.jpg"))
						.getScaledInstance(tileSize, tileSize,
								Image.SCALE_SMOOTH);
				panelBackground = ImageIO.read(ClassLoader
						.getSystemResource("panelBackground.jpg"));

				if (svetty) {
					Image img = ImageIO.read(ClassLoader
							.getSystemResource("ball.png"));
					PLAYER = img.getScaledInstance(tileSize / 2, tileSize / 2,
							Image.SCALE_SMOOTH);
					SVETTY = img.getScaledInstance(Global.WIDTH, Global.HEIGHT,
							Image.SCALE_SMOOTH);
				} else if (PLAYER != null)
					;
				else
					PLAYER = ImageTool.getColoredImage(Color.RED, tileSize / 2,
							tileSize / 2);
			} catch (IOException e) {
				WALL = ImageTool.getColoredImage(Color.BLACK, tileSize,
						tileSize);
				PLAYER = ImageTool.getColoredImage(Color.RED, tileSize / 2,
						tileSize / 2);
			}
		} else {
			WALL = ImageTool.getColoredImage(Color.BLACK, tileSize, tileSize);
			if (PLAYER == null)
				SVETTY = PLAYER = ImageTool.getColoredImage(Color.RED,
						tileSize / 2, tileSize / 2);
		}
		if (heads == null) {
			heads = new BouncingHead[30];
			heads[0] = new BouncingHead(Global.WIDTH / 4,
					Global.HEIGHT / 3 * 2, new Random().nextInt(11), false);
			heads[1] = new BouncingHead(Global.WIDTH / 4 * 3,
					Global.HEIGHT / 3 * 2, new Random().nextInt(11), false);
			for (int i = 2; i < heads.length; i++)
				heads[i] = new BouncingHead(Global.WIDTH / heads.length * i,
						Global.HEIGHT / heads.length * i, 0, true);
		}

		for (BouncingHead a : heads)
			a.setImage(PLAYER);
		GOAL = ImageTool.getColoredImage(Color.GREEN, tileSize, tileSize);
	}

	// ===========================================================================================================================

	private boolean intro = false;

	/**
	 * Update method called by the API
	 */
	@Override
	public void update(long elapsedTime) {
		if (!intro && getState() == GameState.GAME_INTRO) {
			Speak.say("Welcome to " + Global.TITLE + "!");
			intro = true;
		}
		if (getState() == GameState.GAME_INTRO) {
			if (GKeyBoard.isPressed(KeyEvent.VK_SPACE)) {
				if (!MapManager.hasNextMap())
					MapManager.addMap(new MazeMap(SimpleMaze.instance));
				MapManager.loadNextMap();
				start = System.currentTimeMillis();
				setState(GameState.GAME_PLAYING);

			}
			if (!applet)
				gameFont.update(elapsedTime);
			for (BouncingHead a : heads)
				if (a != null)
					a.update(elapsedTime);
		}

		if (!applet && GKeyBoard.isPressed(KeyEvent.VK_ESCAPE)) {
			getGlassPane().setVisible(!getGlassPane().isVisible());
		}
	}

	/**
	 * Render method called by API
	 */
	@Override
	public void render(Graphics2D g) {
		if (!applet)
			g.drawImage(Background.getBackground(), 0, 0, Global.WIDTH,
					Global.HEIGHT, this);
		switch (getState()) {
		case GAME_PAUSED:
			renderPause(g);
			break;
		case GAME_INTRO:
			renderIntro(g);
			break;
		case GAME_PLAYING:
			renderGame(g);
			break;
		case GAME_LOADING:
			break;
		default:
			break;
		}
		if (!applet && getState() != GameState.GAME_PAUSED)
			renderStats(g);
	}

	// =====================================================================================================

	/**
	 * Renders the intro screen
	 * 
	 * @param g
	 *            - Graphics to draw with
	 */
	private void renderIntro(Graphics2D g) {
		for (BouncingHead a : heads)
			g.drawImage(PLAYER, (int) a.getX(), (int) a.getY(), a.getWidth(),
					a.getHeight(), this);
		renderText(svetty ? ("SVETTY MAZE") : ("PHOTO MAZE RUNNER"),
				Global.WIDTH / 2 - 5, Global.HEIGHT / 2 - 62, g, gameFont);
		renderText("PRESS SPACE TO START", Global.WIDTH / 2 - 5,
				Global.HEIGHT / 2 - 16, g, gameFont);
		renderText("PRESS ESC FOR MENU", Global.WIDTH / 2 - 5,
				Global.HEIGHT / 2 + 16, g, gameFont);
	}

	/**
	 * Renders a given string on the screen If this isn't running as an applet,
	 * AnimatedFont will not be used
	 * 
	 * @param string
	 *            - text to draw
	 * @param x
	 *            - x location
	 * @param y
	 *            - y location
	 * @param g
	 *            - graphics to draw with
	 * @param font
	 *            - AnimatedFont to draw with if provided
	 */
	private void renderText(String string, int x, int y, Graphics2D g,
			GFontAnimated font) {
		Rectangle2D bounds = g.getFont().getStringBounds(string,
				g.getFontRenderContext());
		if (!applet)
			font.renderText(string, g, x - (int) bounds.getWidth(), y
					- (int) bounds.getHeight());
		else {
			g.setFont(new Font("Arial", Font.BOLD, 24));
			bounds = g.getFont().getStringBounds(string,
					g.getFontRenderContext());
			g.setColor(new Color(128, 128, 128, 225));
			g.fillRect(x - (int) bounds.getWidth() / 2,
					y - (int) bounds.getHeight() * 2, (int) bounds.getWidth(),
					(int) bounds.getHeight() + 3);
			g.setColor(Color.GREEN);
			g.drawString(string, x - (int) bounds.getWidth() / 2, y
					- (int) bounds.getHeight());
		}
	}

	/**
	 * Renders the pause screen
	 * 
	 * @param g
	 *            - graphics to draw with
	 */
	private void renderPause(Graphics2D g) {
		MapView.render(g);
		g.drawImage(SVETTY, Global.WIDTH / 4, Global.HEIGHT / 4,
				Global.WIDTH / 4 * 2, Global.HEIGHT / 4 * 2, this);
	}

	/**
	 * Renders the game
	 * 
	 * @param g
	 *            - graphics to draw with
	 */
	private void renderGame(Graphics2D g) {
		MapView.render(g);
		if (img != null && camhud && motion) {
			float opacity = 0.5f;
			Composite com = g.getComposite();
			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
					opacity));
			g.drawImage(img, getWidth(), 0, -390, 200, this);
			g.setComposite(com);
		}
		renderText(toDate(System.currentTimeMillis() - start),
				Global.WIDTH / 2 - 5, 70, g, gameFont);
	}

	/**
	 * Renders the statistics
	 * 
	 * @param g
	 *            - graphics to draw with
	 */
	private void renderStats(Graphics2D g) {
		String lvl = "COMPLETED: " + level;
		Rectangle2D bounds = g.getFont().getStringBounds(lvl,
				g.getFontRenderContext());

		String time = Stats.getValue("time");
		String fast = "BEST TIME: "
				+ (!time.equalsIgnoreCase("NA") ? toDate(Long.parseLong(time))
						: time);
		Rectangle2D bounds3 = g.getFont().getStringBounds(fast,
				g.getFontRenderContext());

		String lasttime = Stats.getValue("lasttime");
		String last = "LAST TIME: "
				+ (!lasttime.equalsIgnoreCase("NA") ? toDate(Long
						.parseLong(lasttime)) : lasttime);
		Rectangle2D bounds4 = g.getFont().getStringBounds(last,
				g.getFontRenderContext());

		gameFont.renderText(lvl, g,
				(Global.WIDTH - (int) bounds.getWidth() * 2) - 20,
				(int) bounds.getHeight());
		gameFont.renderText(last, g, 15, 20 + (int) bounds4.getHeight());

		gameFont.renderText(fast, g, 15, (int) bounds3.getHeight());
	}

	// ================================================================================================

	/**
	 * Formats a long value to represent a time
	 * 
	 * @param time
	 *            - time in long
	 * @return formated string
	 */
	private String toDate(long time) {
		Date date = new Date(time);
		DateFormat formatter = new SimpleDateFormat("mm:ss:SSS");
		return formatter.format(date);
	}

	/**
	 * Gets an object with a given char and location
	 */
	@Override
	public GObject getObject(char c, int x, int y) {
		switch (c) {
		case 'W':
			return new Wall(x, y);
		case 'P':
			return new Player(x, y);
		case 'G':
			return new Goal(x, y);
		}
		return null;
	}

	/**
	 * Gets a tile at a location with a given char
	 */
	@Override
	public Tile getTile(char c, int x, int y) {
		return null;
	}

	// ==============================================================

	private static Point center;
	private static Robot robot;

	/**
	 * Checks where the center of motion is on the screen and move the player
	 * accordingly <br>
	 * <br>
	 * <b>example:</b><br>
	 * <br>
	 * <i>(NOTE: the vertical lines are x=0 and the horizontal lines are
	 * y=0)</i><br>
	 * 
	 * <pre>
	 *                         ^                           ^
	 *                         |                           |
	 *     LEFT & UP           |          UP               |       RIGHT & UP
	 *                         |                           |
	 * <-----------------------|--------------------------------------------------->
	 *                         |                           |
	 *                         |                           |
	 *                         |                           |
	 *      LEFT               |      DO NOT MOVE          |         RIGHT
	 *                         |                           |
	 *                         |                           |
	 * <-----------------------|---------------------------|----------------------->
	 *                         |                           |
	 *                         |                           |
	 *                         |                           |
	 *    LEFT & DOWN          |         DOWN              |       DOWN & RIGHT
	 *                         |                           |
	 *                         v                           v
	 * </pre>
	 * 
	 * @param image
	 *            - Image of the current frame
	 * @param point
	 *            - center point of all motion<br>
	 */
	public static void addMotion(BufferedImage image, Point point) {
		if (center == null)
			center = new Point(Global.WIDTH / 2, Global.HEIGHT / 2);
		if (!motion || !focus)
			return;
		img = image;
		double rX = center.getX() - point.getX();
		double rY = center.getY() - point.getY();
		try {
			if (robot == null)
				robot = new Robot();
			// Simulate a key press based on location
			if (rX < 0) {
				robot.keyPress(KeyEvent.VK_LEFT);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_LEFT);
			} else if (rX > 0) {
				robot.keyPress(KeyEvent.VK_RIGHT);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_RIGHT);
			}

			if (rY > 0) {
				robot.keyPress(KeyEvent.VK_UP);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_UP);
			} else if (rY < 0) {
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_DOWN);
			}

		} catch (AWTException e) {
			e.printStackTrace();
		}

	}
}