package net.foxgenesis;

import static org.bytedeco.javacpp.helper.opencv_imgproc.cvFindContours;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.cvAbsDiff;
import static org.bytedeco.javacpp.opencv_core.cvClearMemStorage;
import static org.bytedeco.javacpp.opencv_imgproc.CV_CHAIN_APPROX_SIMPLE;
import static org.bytedeco.javacpp.opencv_imgproc.CV_GAUSSIAN;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RETR_LIST;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RGB2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.CV_THRESH_BINARY;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.cvMinAreaRect2;
import static org.bytedeco.javacpp.opencv_imgproc.cvSmooth;
import static org.bytedeco.javacpp.opencv_imgproc.cvThreshold;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core.CvBox2D;
import org.bytedeco.javacpp.opencv_core.CvContour;
import org.bytedeco.javacpp.opencv_core.CvMemStorage;
import org.bytedeco.javacpp.opencv_core.CvSeq;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.helper.opencv_core.AbstractCvMemStorage;
import org.bytedeco.javacpp.helper.opencv_core.AbstractIplImage;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameGrabber;

import com.gej.core.Global;

/**
 * Cam controls the camera/webcam.<br>
 * Cam draws itself as the image taken from the camera
 * 
 * @see SettingsPane
 * @author Seth
 * 
 */
public class Cam extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6418544207280404448L;
	private Thread thread;
	private BufferedImage picture;
	private OpenCVFrameGrabber grabber;
	private IplImage frame = null;
	private CvMemStorage storage;
	private IplImage image = null;
	private IplImage prevImage = null;
	private IplImage diff = null;
	private boolean stop = false;

	/**
	 * Create a new cam
	 */
	public Cam() {
		super();
		this.setBorder(BorderFactory.createEtchedBorder(1));
		this.setLayout(new BorderLayout());
		grabber = new OpenCVFrameGrabber(0);
		storage = AbstractCvMemStorage.create();
		try {
			grabber.start();
		} catch (FrameGrabber.Exception e1) {
			return;
		}
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				end();
			}
		});
		thread = new Thread() {

			@Override
			public void run() {
				try {
					frame = grabber.grab();

					cvClearMemStorage(storage);

					cvSmooth(frame, frame, CV_GAUSSIAN, 9, 9, 2, 2);
					if (image == null) {
						image = AbstractIplImage.create(frame.width(), frame.height(),
								IPL_DEPTH_8U, 1);
						cvCvtColor(frame, image, CV_RGB2GRAY);
					} else {
						prevImage = AbstractIplImage.create(frame.width(),
								frame.height(), IPL_DEPTH_8U, 1);
						prevImage = image;
						image = AbstractIplImage.create(frame.width(), frame.height(),
								IPL_DEPTH_8U, 1);
						cvCvtColor(frame, image, CV_RGB2GRAY);
					}

					if (diff == null) {
						diff = AbstractIplImage.create(frame.width(), frame.height(),
								IPL_DEPTH_8U, 1);
					}

					if (prevImage != null) {
						BufferedImage img = grabber.grab().getBufferedImage();
						// perform ABS difference
						cvAbsDiff(image, prevImage, diff);
						// do some threshold for wipe away useless details
						cvThreshold(diff, diff, SimpleMaze.sens, 255, CV_THRESH_BINARY);
						Graphics2D g = img.createGraphics();
						g.setColor(Color.RED);
						// recognize contours
						CvSeq contour = new CvSeq(null);
						cvFindContours(diff, storage, contour,
								Loader.sizeof(CvContour.class), CV_RETR_LIST,
								CV_CHAIN_APPROX_SIMPLE);
						ArrayList<CvBox2D> boxes = new ArrayList<>();
						while (contour != null && !contour.isNull()) {
							if (contour.elem_size() > 0) {
								boxes.add(cvMinAreaRect2(contour, storage));
							}
							contour = contour.h_next();
						}
						picture = img;
						Point center = Point.getCenter(boxes
								.toArray(new CvBox2D[] {}));
						for (CvBox2D a : boxes)
							if (a != null) {
								g.setColor(Color.RED);
								g.drawOval((int) a.center().x() - 15, (int) a
										.center().y() - 15, 30, 30);
								g.setColor(Color.CYAN);
								g.drawLine((int) a.center().x(), (int) a
										.center().y(), (int) center.getX(),
										(int) center.getY());
								a.size();
							}
						g.setColor(Color.YELLOW);
						g.drawRect((int) center.getX() - 15,
								(int) center.getY() - 15, 30, 30);
						picture = img;
						SimpleMaze.addMotion(img, center);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				repaint();
				if (!stop)
					run();
				else
					return;
			}
		};
		JButton button = new JButton("Capture");
		button.setBackground(Color.white);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!SimpleMaze.svetty) {
					Image img = picture;
					SimpleMaze.PLAYER = img.getScaledInstance(
							SimpleMaze.tileSize / 2, SimpleMaze.tileSize / 2,
							Image.SCALE_SMOOTH);
					SimpleMaze.SVETTY = img.getScaledInstance(Global.WIDTH,
							Global.HEIGHT, Image.SCALE_SMOOTH);
				}
			}
		});
		add(button, BorderLayout.PAGE_END);
		this.setOpaque(false);
		this.setPreferredSize(new Dimension(390, 200));
		thread.start();
	}

	/**
	 * Gets the image that the camera is currently looking at
	 * 
	 * @return image from the camera
	 */
	public BufferedImage getImage() {
		try {
			return grabber.grab().getBufferedImage();
		} catch (FrameGrabber.Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	/**
	 * Draws the picture to the panel
	 * @param g - graphics to draw with
	 */
	public void paint(Graphics g) {
		if (picture != null)
			g.drawImage(picture, getWidth(), 0, -getWidth(), getHeight(), this);
		super.paint(g);
	}

	/**
	 * Stops any input/connection to the camera
	 */
	@SuppressWarnings("deprecation")
	public void end() {
		try {
			stop = true;
			thread.stop();
			grabber.stop();
			grabber.release();
		} catch (FrameGrabber.Exception e) {
			e.printStackTrace();
		}
	}
}
