package net.foxgenesis;

import com.gej.map.MapInfo;
import com.gej.map.MapLoader;

/**
 * Mazemap that was auto-generated for the SimpleMaze
 * 
 * @see SimpleMaze
 * @see Maze
 * @author Seth
 * 
 */
public class MazeMap extends MapInfo {
	/**
	 * Creates a new MazeMap
	 * 
	 * @param loader
	 *            - MapLoader to load the map
	 */
	public MazeMap(MapLoader loader) {
		setMapLoader(loader);
		tileSize = SimpleMaze.tileSize;
		MapData = Maze.createMaze(SimpleMaze.width, SimpleMaze.height);
	}

}
