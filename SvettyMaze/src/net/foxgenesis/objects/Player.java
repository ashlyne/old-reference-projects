package net.foxgenesis.objects;

import java.awt.event.KeyEvent;
import java.util.Timer;
import java.util.TimerTask;

import net.foxgenesis.SimpleMaze;
import net.foxgenesis.Stats;
import com.gej.core.Game;
import com.gej.core.GameState;
import com.gej.input.GKeyBoard;
import com.gej.map.MapView;
import com.gej.object.GObject;

/**
 * Player object created to move through the maze
 * 
 * @author Seth
 * 
 */
public class Player extends GObject {

	public static final float SPEED = SimpleMaze.tileSize / 8;

	/**
	 * Create a new player at a given location
	 * 
	 * @param x
	 *            - x location
	 * @param y
	 *            - y location
	 */
	public Player(float x, float y) {
		super(SimpleMaze.PLAYER, x, y);
	}

	/**
	 * Called by the api
	 * 
	 * @param elapsedTime
	 *            - elapsed time since last update call
	 */
	@Override
	public void update(long elapsedTime) {
		MapView.follow(this);
		setVelocityX(0);
		setVelocityY(0);
		if (Game.getState() == GameState.GAME_PLAYING) {
			if (GKeyBoard.isPressed(KeyEvent.VK_LEFT)) {
				setVelocityX(-SPEED);
			} else if (GKeyBoard.isPressed(KeyEvent.VK_RIGHT)) {
				setVelocityX(SPEED);
			}
			if (GKeyBoard.isPressed(KeyEvent.VK_UP)) {
				setVelocityY(-SPEED);
			} else if (GKeyBoard.isPressed(KeyEvent.VK_DOWN)) {
				setVelocityY(SPEED);
			}
		}
	}

	/**
	 * Called by the API called when this object collides with another
	 * 
	 * @param other
	 *            - object collided with
	 */
	@Override
	public void collision(GObject other) {
		if (other instanceof Goal) {
			long end = System.currentTimeMillis();
			long current = end - SimpleMaze.start;
			if (!SimpleMaze.applet) {
				SimpleMaze.level++;
				Stats.setStat("lvl", "" + SimpleMaze.level);
				Stats.setStat("lasttime", "" + current);
				if (Stats.getValue("time") != null) {
					String time2 = Stats.getValue("time");
					long time = Long
							.parseLong(time2.equalsIgnoreCase("NA") ? ""
									+ Long.MAX_VALUE : time2);
					if (current < time) {
						Stats.setStat("time", "" + current);
						SimpleMaze.win.start();
					} else
						SimpleMaze.loss.start();
				} else
					Stats.setStat("time", "" + current);
			}
			Game.setState(GameState.GAME_INTRO);
			return;
		}

		if (SimpleMaze.collide && other instanceof Wall) {
			if (Game.getState() == GameState.GAME_PLAYING) {
				Game.setState(GameState.GAME_PAUSED);
				new Timer().schedule(new TimerTask() {

					@Override
					public void run() {
						Game.setState(GameState.GAME_PLAYING);
					}

				}, 1000);
			}
			return;
		}
	}

}
