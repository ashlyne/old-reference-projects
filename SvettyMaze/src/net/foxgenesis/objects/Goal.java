package net.foxgenesis.objects;

import net.foxgenesis.SimpleMaze;

import com.gej.object.GObject;

/**
 * Goal object
 * 
 * @author Seth
 * 
 */
public class Goal extends GObject {
	/**
	 * Create a new goal at a given location
	 * 
	 * @param x
	 *            - x location
	 * @param y
	 *            - y location
	 */
	public Goal(float x, float y) {
		super(SimpleMaze.GOAL, x, y);
	}

}
