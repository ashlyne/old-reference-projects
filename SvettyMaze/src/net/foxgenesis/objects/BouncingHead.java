package net.foxgenesis.objects;

import net.foxgenesis.SimpleMaze;

import com.gej.core.Global;
import com.gej.object.GObject;

/**
 * Bouncing head is an object created for the intro screen that either bounces
 * on the sides or goes up and down
 * 
 * @author Seth
 * 
 */
public class BouncingHead extends GObject {

	private int speed;
	private boolean move;
	private float motionY = 1f;
	private float motionX = 1f;
	private float radius = getWidth() / 2;

	/**
	 * Creates a new bouncing head
	 * 
	 * @param x
	 *            - x location
	 * @param y
	 *            - y location
	 * @param speed
	 *            - how fast should the head move
	 * @param move
	 *            - should the head move around the screen
	 */
	public BouncingHead(float x, float y, int speed, boolean move) {
		super(SimpleMaze.PLAYER, x, y);
		this.speed = speed;
		this.move = move;
	}

	private float update = 0;

	@Override
	/**
	 * called by the API
	 * @param a - elapsed time
	 */
	public void update(long a) {
		if (!move) {
			float v = (float) Math.sin(update += update < 360 ? (0.3)
					: (-(update + 3))) * speed;
			float c = (float) Math.cos(update += update < 360 ? (0.3)
					: (-(update + 3))) * speed;
			this.setY(getY() + v);
			this.setX(getX() + c);
		} else {
			float centerX = getX() + (getWidth() / 2);
			float centerY = getY() + (getHeight() / 2);

			if (outOfBounds(centerX + motionX, Global.WIDTH))
				motionX = -motionX;
			if (outOfBounds(centerY + motionY, Global.HEIGHT))
				motionY = -motionY;
			setY(getY() + motionY);
			setX(getX() + motionX);
		}
	}

	/**
	 * Checks if a number is beyond the maximum value or below the minimum(0)
	 * 
	 * @param x
	 *            - number to check
	 * @param maximim
	 *            - maximum value of the number
	 * @return returns true if number is out of range, false otherwise
	 */
	private boolean outOfBounds(float x, float maximim) {
		if (x + radius > maximim)
			return true;
		else if (x - radius < 0)
			return true;
		else
			return false;
	}
}
