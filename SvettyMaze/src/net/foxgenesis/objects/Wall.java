package net.foxgenesis.objects;

import net.foxgenesis.SimpleMaze;

import com.gej.object.GObject;

/**
 * Wall object created to block player movement
 * 
 * @author Seth
 * 
 */
public class Wall extends GObject {
	/**
	 * Create a new wall object at a given location
	 * 
	 * @param x
	 *            - x location
	 * @param y
	 *            - y location
	 */
	public Wall(float x, float y) {
		super(SimpleMaze.WALL, x, y);
		setSolid(true);
	}

}
