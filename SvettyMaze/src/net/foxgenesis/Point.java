package net.foxgenesis;

import org.bytedeco.javacpp.opencv_core.CvBox2D;

/**
 * Point holds an x and y value
 * 
 * @author Seth
 */
public class Point {
	private double x, y;

	/**
	 * Creates a new point at a given location
	 * 
	 * @param x
	 *            - x location
	 * @param y
	 *            - y location
	 */
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Gets the x location of the point
	 * 
	 * @return x location
	 */
	public double getX() {
		return x;
	}

	/**
	 * Gets the y location of the point
	 * 
	 * @return y location
	 */
	public double getY() {
		return y;
	}

	/**
	 * Gets the center point of a list of points PRECONDITION: boxes > 0
	 * 
	 * @param boxes
	 *            list of points
	 * @return center of all points
	 */
	public static Point getCenter(CvBox2D[] boxes) {
		double sumX = 0, sumY = 0;
		for (CvBox2D box : boxes) {
			sumX += box.center().x();
			sumY += box.center().y();
		}
		return new Point(sumX / boxes.length, sumY / boxes.length);
	}
}
