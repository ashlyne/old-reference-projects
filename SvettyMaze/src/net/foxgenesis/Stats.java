package net.foxgenesis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.gej.core.Global;

/**
 * Stats generates the stats file to store information about the game
 * 
 * @author Seth
 * 
 */
public final class Stats {
	private static File file;
	private static HashMap<String, String> hash;

	/**
	 * Called by the java runtime
	 */
	static {
		file = new File("stats");
		if (!file.exists())
			try {
				System.out.println("no stats found. creating one for you!");
				file.createNewFile();
				hash = new HashMap<>();
				setStat("time", "NA");
				setStat("lvl", "0");
				setStat("fullscreen", "false");
				setStat("width", "32");
				setStat("height", "32");
				setStat("zoom", "32");
				setStat("svetty", "false");
				setStat("collide", "false");
				setStat("motion", "true");
				setStat("camhud", "true");
				setStat("lasttime", "NA");
				setStat("sens","60");
				updateFile();
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("Failed to create stats file!");
			}
		else
			System.out.println("found stats!");
		hash = new HashMap<>();
		updateStats();
	}

	/**
	 * Updates SimpleMaze's values
	 */
	public static void updateMain() {
		SimpleMaze.level = parseInt(hash.get("lvl"));
		SimpleMaze.height = parseInt(hash.get("height"));
		SimpleMaze.width = parseInt(hash.get("width"));
		SimpleMaze.svetty = parseBoolean(hash.get("svetty"));
		SimpleMaze.collide = parseBoolean(hash.get("collide"));
		Global.FULLSCREEN = parseBoolean(hash.get("fullscreen"));
		SimpleMaze.tileSize = parseInt(hash.get("zoom"));
		SimpleMaze.motion = parseBoolean(hash.get("motion"));
		SimpleMaze.sens = parseInt(hash.get("sens"));
		SimpleMaze.camhud = parseBoolean(hash.get("camhud"));
	}

	/**
	 * Gets an integer from a string
	 * 
	 * @param string
	 *            text to turn into a number
	 * @return number formed from the string
	 */
	private static int parseInt(String string) {
		return Integer.parseInt(string.replaceAll("[^\\d.]", ""));
	}

	/**
	 * Gets a boolean from a string
	 * 
	 * @param string
	 *            text to parse into a boolean
	 * @return boolean formed from the string
	 */
	private static boolean parseBoolean(String string) {
		return string.equalsIgnoreCase("true");
	}

	/**
	 * Sets a statistic
	 * 
	 * @param key
	 *            Key to hold the value
	 * @param value
	 *            Value to be held
	 */
	public static void setStat(String key, String value) {
		hash.put(key, value);
		updateFile();
	}

	/**
	 * Gets a value from the statistics
	 * 
	 * @param key
	 *            Key that holds the value
	 * @return value that was held by the key or null if no value was found
	 */
	public static String getValue(String key) {
		return hash.get(key);
	}

	/**
	 * Updates the file with the current statistics
	 */
	private synchronized static void updateFile() {
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(file));
			out.write(hashToString(hash));

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Gets the current text in the file
	 * 
	 * @return text contained inside the file
	 */
	private synchronized static String[] getText() {
		ArrayList<String> text = new ArrayList<>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line;
			while ((line = in.readLine()) != null)
				text.add(line);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text.toArray(new String[] {});
	}

	/**
	 * Forms a string from a hashmap
	 * 
	 * @param map
	 *            hashmap to turn into a string
	 * @return strings created from the hashmap
	 */
	private static String hashToString(HashMap<String, String> map) {
		String output = "";
		String[] keys = map.keySet().toArray(new String[] {});
		String[] values = map.values().toArray(new String[] {});
		for (int i = 0; i < keys.length; i++)
			output += keys[i] + ":" + values[i] + "\n";
		return output;
	}

	/**
	 * Updates the hashmap
	 */
	private static void updateStats() {
		String[] stats = getText();
		for (String a : stats) {
			String[] i = a.split(":");
			hash.put(i[0], i[1]);
		}
	}
}
