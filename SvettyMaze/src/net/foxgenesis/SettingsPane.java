package net.foxgenesis;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import com.gej.core.Global;
import com.gej.graphics.Background;
import com.gej.util.ImageTool;

/**
 * SettingsPane is the glass pane menu that is toggled with ESC
 * 
 * @author Seth
 * 
 */
public class SettingsPane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5443752980519095513L;

	/**
	 * Creates a new settings pane with a given runnable
	 * 
	 * @param run
	 *            - runnable to be run when apply is clicked
	 */
	public SettingsPane(final Runnable run) {
		final JPanel center = new JPanel();
		this.setLayout(new BorderLayout());
		center.setLayout(new GridLayout(0, 1));
		center.setOpaque(false);
		giveKeyListener(center);
		add(center, BorderLayout.LINE_START);
		JPanel bottom = new JPanel();
		giveKeyListener(bottom);
		bottom.setLayout(new FlowLayout(FlowLayout.LEFT));
		add(bottom, BorderLayout.PAGE_END);
		JButton button = new JButton("Apply");

		final JTextField h = new JTextField(10);
		giveKeyListener(h);
		h.setText(Stats.getValue("height"));
		center.add(newOption(h, "Maze Height(10-35): "));

		final JTextField w = new JTextField(10);
		giveKeyListener(w);
		w.setText(Stats.getValue("width"));
		center.add(newOption(w, "Maze Width(10-35): "));

		final JTextField z = new JTextField(10);
		giveKeyListener(z);
		z.setText(Stats.getValue("zoom"));
		center.add(newOption(z, "Zoom: "));

		final JCheckBox s = new JCheckBox();
		giveKeyListener(s);
		s.setBackground(null);
		center.add(newOption(s, "Svetty Mode: "));
		s.setSelected(Stats.getValue("svetty").equalsIgnoreCase("true"));

		final JCheckBox f = new JCheckBox();
		giveKeyListener(f);
		center.add(newOption(f, "Fullscreen: "));
		f.setSelected(Stats.getValue("fullscreen").equalsIgnoreCase("true"));

		final JCheckBox co = new JCheckBox();
		giveKeyListener(co);
		center.add(newOption(co, "Wall Collide Penalty: "));
		co.setSelected(Stats.getValue("collide").equalsIgnoreCase("true"));

		final JCheckBox m = new JCheckBox();
		giveKeyListener(m);
		final JCheckBox hud = new JCheckBox();
		giveKeyListener(hud);
		JPanel sensPanel = newOption(null, "Sensitivity: ");
		sensPanel.setOpaque(false);
		final JSlider sens = new JSlider();
		giveKeyListener(sens);
		sens.setMinimum(0);
		sens.setMaximum(100);
		sens.setValue(toInt(Stats.getValue("sens")));
		sens.setPreferredSize(new Dimension(100,10));
		final JLabel sensInfo = new JLabel("" + sens.getValue());
		sensPanel.add(sensInfo);
		sensPanel.add(sens);
		sens.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				SimpleMaze.sens = sens.getValue();
				Stats.setStat("sens", "" + sens.getValue());
				sensInfo.setText("" + sens.getValue());
			}

		});
		sens.setSnapToTicks(true);
		sens.setInputVerifier(new InputVerifier() {

			@Override
			public boolean verify(JComponent arg0) {
				return false;
			}

		});
		final JPanel hudP = newOption(hud, "Draw Camera over game: ");
		giveKeyListener(hudP);
		center.add(newOption(m, "Motion Compatability: "));
		m.setSelected(Stats.getValue("motion").equalsIgnoreCase("true"));
		m.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (m.isSelected()) {
					hudP.setVisible(true);
					sens.setVisible(true);
				}
				else {
					hudP.setVisible(false);
					sens.setVisible(false);
				}
			}
		});
		center.add(hudP);
		center.add(sensPanel);
		hud.setSelected(Stats.getValue("camhud").equalsIgnoreCase("true"));

		final JFileChooser back = new JFileChooser();
		back.setBackground(Color.white);
		back.setFileFilter(new FileFilter() {

			@Override
			public boolean accept(File arg0) {
				return arg0.getAbsolutePath().endsWith(".gif")
						|| arg0.getAbsolutePath().endsWith(".jpg")
						|| arg0.getAbsolutePath().endsWith(".png");
			}

			@Override
			public String getDescription() {
				return "Image Files (.gif, .jpg, .png)";
			}

		});

		JButton b = new JButton("Choose Background Image");
		giveKeyListener(b);
		b.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int action = back.showOpenDialog(SimpleMaze.instance);
				if (action == JFileChooser.APPROVE_OPTION)
					try {
						Background.setBackground(ImageIO.read(back
								.getSelectedFile()));
					} catch (IOException e1) {
					}
			}
		});
		b.setBackground(Color.white);
		final JFileChooser wall = new JFileChooser();
		wall.setBackground(Color.white);
		wall.setFileFilter(new FileFilter() {

			@Override
			public boolean accept(File arg0) {
				return arg0.getAbsolutePath().endsWith(".gif")
						|| arg0.getAbsolutePath().endsWith(".jpg")
						|| arg0.getAbsolutePath().endsWith(".png");
			}

			@Override
			public String getDescription() {
				return "Image Files (.gif, .jpg, .png)";
			}

		});
		JButton wa = new JButton("Choose Wall Image");
		giveKeyListener(wa);
		wa.setBackground(Color.white);
		wa.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int action = wall.showOpenDialog(SimpleMaze.instance);
				if (action == JFileChooser.APPROVE_OPTION)
					try {
						SimpleMaze.WALL = ImageIO
								.read(wall.getSelectedFile())
								.getScaledInstance(SimpleMaze.tileSize,
										SimpleMaze.tileSize, Image.SCALE_SMOOTH);
						;
					} catch (IOException e1) {
						e1.printStackTrace();
					}
			}
		});

		final Cam cam = new Cam();
		giveKeyListener(cam);
		add(cam, BorderLayout.LINE_END);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				int he = getValue(h);
				SimpleMaze.height = he > 10 && he <= 35 ? (he)
						: (SimpleMaze.height);
				Stats.setStat("height", he + "");

				int we = getValue(w);
				SimpleMaze.width = we > 10 && we <= 35 ? (we)
						: (SimpleMaze.width);
				Stats.setStat("width", we + "");

				int ze = getValue(z);
				SimpleMaze.tileSize = ze > 0 ? (ze) : (SimpleMaze.tileSize);
				Stats.setStat("zoom", ze + "");

				SimpleMaze.svetty = s.isSelected();
				if (Stats.getValue("svetty").equalsIgnoreCase("true")
						&& !s.isSelected())
					SimpleMaze.PLAYER = ImageTool.getColoredImage(Color.RED,
							SimpleMaze.tileSize / 2, SimpleMaze.tileSize / 2);
				Stats.setStat("svetty", s.isSelected() + "");
				Global.TITLE = SimpleMaze.svetty ? ("Svetty Maze")
						: ("Photo Maze Runner");

				SimpleMaze.collide = co.isSelected();
				Stats.setStat("collide", co.isSelected() + "");

				Global.FULLSCREEN = f.isSelected();
				Stats.setStat("fullscreen", f.isSelected() + "");

				SimpleMaze.motion = m.isSelected();
				Stats.setStat("motion", m.isSelected() + "");

				SimpleMaze.camhud = hud.isSelected();
				Stats.setStat("camhud", hud.isSelected() + "");

				Stats.setStat("sens", "" + sens.getValue());
				SimpleMaze.sens = sens.getValue();
				run.run();
			}
		});
		button.setBackground(Color.WHITE);
		bottom.setOpaque(false);
		center.add(button);
		bottom.add(wa);
		bottom.add(b);
		giveKeyListener(this);
	}

	@Override
	/**
	 * Called by the component when it needs to be painted
	 * @param g - graphics to draw with
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(SimpleMaze.panelBackground, 0, 0, this.getWidth(),
				this.getHeight(), this);
	}

	/**
	 * Creates a new panel for a component with a given label leave blank for no
	 * label
	 * 
	 * @param field
	 *            - component to hold
	 * @param label
	 *            - label for the component
	 * @return panel with component and label inside
	 */
	private JPanel newOption(JComponent field, String label) {
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		if (label != "") {
			JLabel l = new JLabel(label);
			if(field != null)
				field.setBackground(Color.WHITE);
			panel.add(l);
		}
		if(field != null)
			panel.add(field);
		panel.setOpaque(false);
		return panel;
	}

	/**
	 * Gets an integer from a textfield and removes any non-number characters
	 * 
	 * @param field
	 *            - textfield to get value from
	 * @return integer parsed from the textfield
	 */
	private int getValue(JTextField field) {
		String text = field.getText();
		text = text.replaceAll("[^\\d.]", "");
		if (text.equals(""))
			return -1;
		else
			return Integer.parseInt(text);
	}

	private int toInt(String string) {
		string = string.replaceAll("[^\\d.]", "");
		if (string.equals(""))
			return 60;
		else
			return Integer.parseInt(string);
	}

	private void giveKeyListener(JComponent component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					SimpleMaze.instance.getGlassPane().setVisible(
							!SimpleMaze.instance.getGlassPane().isVisible());
				}
			}
		});
	}
} 