package net.foxgenesis;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.helper.opencv_core.AbstractCvMemStorage;
import org.bytedeco.javacpp.helper.opencv_core.AbstractIplImage;
import org.bytedeco.javacv.*;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

/**
 * Small program created to test motion.<br>
 * Class contains adapted code. <br>
 * Program detects motion from an image<br>
 * 
 * <b>Creator of original:</b><i> Angelo Marchesin
 * <a>marchesin.angelo@gmail.com</a> </i>
 * 
 * @author Seth
 * 
 */
public class MotionDetector {
	public static void main(String[] args) {
		try {
			OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
			grabber.start();

			IplImage frame = grabber.grab();
			IplImage image = null;
			IplImage prevImage = null;
			IplImage diff = null;

			CanvasFrame canvasFrame = new CanvasFrame("Some Title");
			canvasFrame.setCanvasSize(frame.width(), frame.height());

			CvMemStorage storage = AbstractCvMemStorage.create();

			while (canvasFrame.isVisible() && (frame = grabber.grab()) != null) {
				cvClearMemStorage(storage);

				cvSmooth(frame, frame, CV_GAUSSIAN, 9, 9, 2, 2);
				if (image == null) {
					image = AbstractIplImage.create(frame.width(), frame.height(),
							IPL_DEPTH_8U, 1);
					cvCvtColor(frame, image, CV_RGB2GRAY);
				} else {
					prevImage = AbstractIplImage.create(frame.width(), frame.height(),
							IPL_DEPTH_8U, 1);
					prevImage = image;
					image = AbstractIplImage.create(frame.width(), frame.height(),
							IPL_DEPTH_8U, 1);
					cvCvtColor(frame, image, CV_RGB2GRAY);
				}

				if (diff == null) {
					diff = AbstractIplImage.create(frame.width(), frame.height(),
							IPL_DEPTH_8U, 1);
				}

				if (prevImage != null) {
					BufferedImage img = grabber.grab().getBufferedImage();
					// perform ABS difference
					cvAbsDiff(image, prevImage, diff);
					// do some threshold for wipe away useless details
					cvThreshold(diff, diff, 64, 255, CV_THRESH_BINARY);
					Graphics2D g = img.createGraphics();
					g.setColor(Color.RED);
					// recognize contours
					CvSeq contour = new CvSeq(null);
					canvasFrame.showImage(diff);
					cvFindContours(diff, storage, contour,
							Loader.sizeof(CvContour.class), CV_RETR_LIST,
							CV_CHAIN_APPROX_SIMPLE);
					ArrayList<CvBox2D> boxes = new ArrayList<>();
					while (contour != null && !contour.isNull()) {
						if (contour.elem_size() > 0) {
							boxes.add(cvMinAreaRect2(contour, storage));
						}
						contour = contour.h_next();
					}
					g.setColor(Color.GREEN);
					g.drawLine(img.getWidth() / 3, 0, img.getWidth() / 3,
							img.getHeight());
					g.drawLine((img.getWidth() / 3) * 2, 0,
							(img.getWidth() / 3) * 2, img.getHeight());
					g.drawLine(0, img.getHeight() / 3, img.getWidth(),
							img.getHeight() / 3);
					g.drawLine(0, (img.getHeight() / 3) * 2, img.getWidth(),
							(img.getHeight() / 3) * 2);
					Point center = Point.getCenter(boxes
							.toArray(new CvBox2D[] {}));
					for (CvBox2D a : boxes)
						if (a != null) {
							g.setColor(Color.RED);
							g.drawOval((int) a.center().x() - 15, (int) a
									.center().y() - 15, 30, 30);
							g.drawOval((int) a.center().x() - 10, (int) a
									.center().y() - 10, 20, 20);
							g.setColor(Color.CYAN);
							g.drawLine((int) a.center().x(), (int) a.center()
									.y(), (int) center.getX(), (int) center
									.getY());
							a.size();
						}
					g.setColor(Color.YELLOW);
					if (boxes.size() > 0)
						g.drawRect((int) center.getX() - 15,
								(int) center.getY() - 15, 30, 30);
					addMotion(img, center);
				}
			}
			grabber.stop();
			canvasFrame.dispose();
		} catch (Exception e) {
		}
	}

	private static Point center;
	private static Robot robot;

	/**
	 * Takes in motion from the motion detector and applies to the game
	 * 
	 * @param image
	 *            - Image of the current frame
	 * @param point
	 *            - center point of all motion
	 */
	public static void addMotion(BufferedImage image, Point point) {
		if (center == null)
			center = new Point(image.getWidth() / 2, image.getHeight() / 2);
		double rX = center.getX() - point.getX();
		double rY = center.getY() - point.getY();

		try {
			if (robot == null)
				robot = new Robot();
			// Simulate a key press
			if (rX < 0) {
				robot.keyPress(KeyEvent.VK_LEFT);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_LEFT);
			} else if (rX > 0) {
				robot.keyPress(KeyEvent.VK_RIGHT);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_RIGHT);
			}

			if (rY > 0) {
				robot.keyPress(KeyEvent.VK_UP);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_UP);
			} else if (rY < 0) {
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_DOWN);
			}

		} catch (AWTException e) {
			e.printStackTrace();
		}

	}
}
