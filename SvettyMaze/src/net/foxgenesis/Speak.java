package net.foxgenesis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

/**
 * Speak is a text to speak program
 * 
 * @author Seth
 * 
 */
public final class Speak {

	private static File file;

	/**
	 * Lists all the available voices
	 */
	public static void listAllVoices() {
		System.out.println();
		System.out.println("All voices available:");
		VoiceManager voiceManager = VoiceManager.getInstance();
		Voice[] voices = voiceManager.getVoices();
		for (int i = 0; i < voices.length; i++) {
			System.out.println("    " + voices[i].getName() + " ("
					+ voices[i].getDomain() + " domain)");
		}
	}

	private static Voice voice;
	static {
		file = new File("script.txt");
		VoiceManager voiceManager = VoiceManager.getInstance();
		listAllVoices();
		voice = voiceManager.getVoice("kevin16");
		if (voice == null) {
			System.err.println("Failed to find voice!");
			System.err.println("Will not use voices");
		}
		if (voice != null)
			voice.allocate();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (voice != null) {
					Speak.say("Thank you for playing");
					voice.deallocate();
				}
			}
		});
	}

	/**
	 * Say a string
	 * 
	 * @param text
	 *            - text to say
	 */
	public static void say(String text) {
		if (voice != null)
			voice.speak(text);
	}

	private static String[] getText() {
		ArrayList<String> text = new ArrayList<>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line;
			while ((line = in.readLine()) != null)
				text.add(line);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text.toArray(new String[] {});
	}

	private static String[] comments = null;

	public static void sayWittyComment() {
		if (comments == null)
			comments = getText();
		if (voice != null)
			voice.speak(comments[new Random().nextInt(comments.length)]);
	}
}
