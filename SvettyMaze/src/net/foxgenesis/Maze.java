package net.foxgenesis;

import java.util.Random;
import java.util.Vector;

/**
 * Class auto-generates a new maze.<br>
 * Algorithm used for generation is a randomized version of Prim's algorithm.<br>
 * This class contains adapted code.<br>
 * Forgot where i got it from xD
 * 
 * @see MazeMap
 * @author Seth
 * 
 */
public final class Maze {
	/**
	 * Generate a new maze with a given size
	 * 
	 * @param width
	 *            - width of the maze
	 * @param height
	 *            - height of the maze
	 * @return string representing the maze (W = wall)
	 */
	public static String createMaze(int width, int height) {
		int[][] mySquares = new int[width][height];
		for (int i = 1; i < width - 1; i++) {
			for (int j = 1; j < height - 1; j++) {
				if ((i % 2 == 1) || (j % 2 == 1)) {
					mySquares[i][j] = 1;
				}
			}
		}
		mySquares[0][1] = 1;
		for (int i = 1; i < mySquares.length - 1; i++) {
			for (int j = 1; j < mySquares[i].length - 1; j++) {
				if ((i + j) % 2 == 1) {
					mySquares[i][j] = 0;
				}
			}
		}
		for (int i = 1; i < mySquares.length - 1; i += 2) {
			for (int j = 1; j < mySquares[i].length - 1; j += 2) {
				mySquares[i][j] = 3;
			}
		}
		Vector<int[]> possibleSquares = new Vector<int[]>(mySquares.length
				* mySquares[0].length);
		int[] startSquare = new int[2];
		startSquare[0] = getRandomInt(mySquares.length / 2) * 2 + 1;
		startSquare[1] = getRandomInt(mySquares[0].length / 2) * 2 + 1;
		mySquares[startSquare[0]][startSquare[1]] = 2;
		possibleSquares.addElement(startSquare);
		while (possibleSquares.size() > 0) {
			int chosenIndex = getRandomInt(possibleSquares.size());
			int[] chosenSquare = possibleSquares.elementAt(chosenIndex);
			mySquares[chosenSquare[0]][chosenSquare[1]] = 1;
			possibleSquares.removeElementAt(chosenIndex);
			link(chosenSquare, possibleSquares, mySquares);
		}
		possibleSquares = null;

		for (int i = 0; i < mySquares.length; i++) {
			mySquares[i][0] = 0;
			mySquares[0][i] = 0;
			mySquares[i][mySquares.length - 1] = 0;
			mySquares[mySquares.length - 1][i] = 0;
		}

		String output = "";
		for (int i = 0; i < mySquares.length; i++) {
			for (int j = 0; j < mySquares.length; j++) {
				if (i == 1 && j == 1)
					output += "P";
				else if (j == mySquares.length - 3 && i == mySquares.length - 3)
					output += "G";
				else
					output += mySquares[i][j] == 0 ? ("W") : (" ");
			}
			output += "\n";
		}
		return output;
	}

	/**
	 * Generates a new random number
	 * 
	 * @param i
	 *            - maximum value
	 * @return random integer
	 */
	private static int getRandomInt(int i) {
		return new Random().nextInt(i);
	}

	/**
	 * Links spaces together so the maze can be completed
	 * 
	 * @param chosenSquare
	 * @param possibleSquares
	 * @param mySquares
	 */
	private static void link(int[] chosenSquare, Vector<int[]> possibleSquares,
			int[][] mySquares) {
		int linkCount = 0;
		int i = chosenSquare[0];
		int j = chosenSquare[1];
		int[] links = new int[8];
		if (i >= 3) {
			if (mySquares[i - 2][j] == 1) {
				links[2 * linkCount] = i - 1;
				links[2 * linkCount + 1] = j;
				linkCount++;
			} else if (mySquares[i - 2][j] == 3) {
				mySquares[i - 2][j] = 2;
				int[] newSquare = new int[2];
				newSquare[0] = i - 2;
				newSquare[1] = j;
				possibleSquares.addElement(newSquare);
			}
		}
		if (j + 3 <= mySquares[i].length) {
			if (mySquares[i][j + 2] == 3) {
				mySquares[i][j + 2] = 2;
				int[] newSquare = new int[2];
				newSquare[0] = i;
				newSquare[1] = j + 2;
				possibleSquares.addElement(newSquare);
			} else if (mySquares[i][j + 2] == 1) {
				links[2 * linkCount] = i;
				links[2 * linkCount + 1] = j + 1;
				linkCount++;
			}
		}
		if (j >= 3) {
			if (mySquares[i][j - 2] == 3) {
				mySquares[i][j - 2] = 2;
				int[] newSquare = new int[2];
				newSquare[0] = i;
				newSquare[1] = j - 2;
				possibleSquares.addElement(newSquare);
			} else if (mySquares[i][j - 2] == 1) {
				links[2 * linkCount] = i;
				links[2 * linkCount + 1] = j - 1;
				linkCount++;
			}
		}
		if (i + 3 <= mySquares.length) {
			if (mySquares[i + 2][j] == 3) {
				mySquares[i + 2][j] = 2;
				int[] newSquare = new int[2];
				newSquare[0] = i + 2;
				newSquare[1] = j;
				possibleSquares.addElement(newSquare);
			} else if (mySquares[i + 2][j] == 1) {
				links[2 * linkCount] = i + 1;
				links[2 * linkCount + 1] = j;
				linkCount++;
			}
		}
		if (linkCount > 0) {
			int linkChoice = getRandomInt(linkCount);
			int linkX = links[2 * linkChoice];
			int linkY = links[2 * linkChoice + 1];
			mySquares[linkX][linkY] = 1;
			int[] removeSquare = new int[2];
			removeSquare[0] = linkX;
			removeSquare[1] = linkY;
			possibleSquares.removeElement(removeSquare);
		}
	}
}
